<?php
/**
 * The Template for displaying Category pages
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-3">
			<?php if ( is_active_sidebar( 'sidebar-news-page' ) ) : ?>
			<div id="first" class="widget-area" role="complementary">		
				<?php dynamic_sidebar( 'sidebar-news-page' ); ?>
			</div>
			<?php endif; ?>
		</div>
	

		<div class="col-sm-9">
			<?php echo get_breadcrumb('category'); ?>
			<h2>Category: <?php echo single_cat_title( '', false ); ?></h2>		
			
			<div class="feed">
				<?php display_post_feed($post,5); ?>
			</div>
		</div>	
			
	</div><!-- /.row -->




	
	
	
</div>
<?php get_footer(); ?>
		
		

	