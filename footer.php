

<?php if ( is_active_sidebar( 'footer-row-1' ) ) : ?>
<div class="footer-row footer-row-1">
	<div id="first" class="widget-area" role="complementary">		
		<?php dynamic_sidebar( 'footer-row-1' ); ?>
	</div>
</div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-row-2' ) ) : ?>
<div class="footer-row footer-row-2">
	<div id="second" class="container widget-area" role="complementary">
		<div class="row">
		<?php dynamic_sidebar( 'footer-row-2' ); ?>
		</div>
	</div>
</div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-row-3' ) ) : ?>
<div class="footer-row footer-row-2">
	<div id="third" class="container widget-area" role="complementary">
		<div class="row">
		<?php dynamic_sidebar( 'footer-row-3' ); ?>
		</div>
	</div>
</div>
<?php endif; ?>



	




<footer>

	<div class="container">
		<div class="row">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="footer-navbar-collapse">
			<?php
			
			//$test = array();
			// hold all the strings to later make footer columns
			$footer_str = array();
			// get all parent pages
			$parents = om_get_parent_pages('page');
			// loop
			foreach( $parents as $parent ) {
				// creating heading
				$str = '<ul><li class="h4"><a href="'. 
							get_page_link($parent->ID) .'">'. $parent->post_title .'</a></li>';
				// if there are children
				if ($children = om_get_child_pages($parent->ID,'page')){
					// loop
					foreach($children as $child){
						// create child links
						$str .= '<li class="h5"><a href="'. get_page_link($child->ID) .'">'. 
									$child->post_title .'</a>'.
									//' '.$child->menu_order.
									'</li>';
					}
				}
				array_push($footer_str,$str."</ul>");
				//array_push($test,$children);
			}
			//print '<pre>';
			//print_r($test);
			//print '</pre>';
			
			
			
			
			$form .= '<span class="social_links">'. social_links() .'</span>';
			
			
			/*
			$form .= '
				<li class="subscribe-form">Subscribe to our newsletter</li>
				<li class="input-group">
				    <input type="email" value="" placeholder="E-mail address" name="email" class="required email form-control" id="email">
				    <span class="input-group-btn">
					<input type="submit" class="button btn btn-default" value="Go" name="subscribe" id="subscribe">
				    </span>
				</li>';
			*/
			array_push($footer_str,$form);
			
			// put n+ lists of links in n columns
			// count the string and divide by # of columns
			$limit = ceil(strlen(implode(' ',$footer_str))/4);
			
			$col = 1;						// first column
			$count = 0;						// counter
			$col_strs = array_fill(1,4,'');	// create array for column strings
			foreach($footer_str as $list){	// for each nav list
				// concat new list to current list
				$col_strs[$col] =  $col_strs[$col].' '.$list;
				// total strlen
				$count += strlen($col_strs[$col]);	
				// if we are over the limit
				if ($count >= $limit-20){	
					$col++;		// then move to the next column
					$count = 0; // and reset count
					// reset count based on leftovers
					$limit = ceil(strlen(implode(' ',$footer_str))/ 4-$col);
				} 
			}
			foreach($col_strs as $str){ ?>
			<div class="col-sm-3"><?php print $str ?></div>
			<?php } ?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="<?php echo get_theme_mod( 'footerlogo_textbox') ?>" class="img-responsive center-block footer-logo" alt="FSU CVATD logo footer">
			</div>
		</div>
	</div>

        <script>
            $('a[rel=lightbox], .gallery-icon a').colorbox({rel:'gal', scalePhotos: true, maxWidth: $(window).width()*.8, maxHeight: $(window).height()*.8 });
        </script>	

	<?php wp_footer(); ?>
</footer>
</body>
</html>