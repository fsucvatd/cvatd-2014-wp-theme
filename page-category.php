<?php
/**
   Template Name: Category Page
 * The Template for displaying all posts from a specified category.
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-3">
			<?php print_subnav($post); ?>
		</div>
	

		<div class="col-sm-9">
			<?php echo get_breadcrumb(); ?>
			<h2><?php print get_the_title($post->ID); ?></h2>			
			
			<div class="feed">
				<?php display_post_feed($post,5); ?>
			</div>
		</div>	
			
	</div><!-- /.row -->
	
		
</div>

<?php get_footer(); ?>