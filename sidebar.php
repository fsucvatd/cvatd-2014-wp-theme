<?php
/**
 * The sidebar containing the main widget area
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-news-page' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-news-page' ); ?>
		</div>
	<?php endif; ?>