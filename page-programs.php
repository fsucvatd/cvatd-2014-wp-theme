<?php
/**
   Template Name: Programs Page
 * Template for displaying programs. Hides the programs widget.
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-3">
			<?php print_subnav($post); ?>
		</div>
	

		<div class="col-sm-9">
			
			<?php echo get_breadcrumb(); ?>
		
			<h2><?php print get_the_title($post->ID); ?></h2>
			
			

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
  

  </div>
</div>

<?php get_footer("hideprograms"); ?>