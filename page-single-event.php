<?php
/**
   Template Name: Singe Event
 * The Template for displaying details for single events
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-8">
			<?php echo get_breadcrumb(); ?>
			<h2>
                            <span class="social_links pull-right" >
				<?php echo social_links( $post->ID, $_GET['eventid'] ); ?>
                            </span>
                            
                            <?php print get_the_title($post->ID); ?>
                        
                        </h2>
			
			

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
				
				<?php include get_template_directory() . "/assets/events/event-single.php"; ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
		
		
	
		<div class="col-sm-4">
			<div class="feed">
				<div class="row feed-col-header">
					<h4>Upcoming Events</h4>
				</div>
				
					
				<?php if ( is_active_sidebar( 'sidebar-events-page' ) ) : ?>
				<div class="sidebar">
					<div class="widget-area" role="complementary">
						<?php dynamic_sidebar( 'sidebar-events-page' ); ?>
					</div>
				</div>
				<?php endif; ?>
			
			</div>
		</div>
  

  </div>
</div>

<?php get_footer(); ?>