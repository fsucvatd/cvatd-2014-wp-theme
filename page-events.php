<?php
/**
   Template Name: Events
 * The Template for displaying all facilities & performance spaces.
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-8">
			<?php echo get_breadcrumb(); ?>
			<h2><?php print get_the_title($post->ID); ?></h2>
			
		

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
				
				<?php include get_template_directory() . "/assets/events/events-all.php"; ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
  
  
  	
  
  		<div class="col-sm-4">
		
		<div class="feed">
			<div class="row feed-col-header">
				<h4>Upcoming Events</h4>
			</div>
			
				
				
			<!-- right column -->
			<div class="right33">
			
				<!-- start MINI CALENDAR -->
				<div id="cal1" class="calendar-mini">
				
					<!-- start CALENDAR NAME & ARROW -->
					<div class="month-name">
						<a class="previous_arrow" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg" alt="right arrow;" onClick="calendar.changeMonth('previousMonth', $('#program').val(), $('#venue').val())" onMouseOver="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow_hover.jpg'" onMouseOut="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg'" width="19" height="19" /></a>
						<h4 class="month-name-text">
							<script>
							/* generate a query string, for "month" label hyperlink */
							queryStringURL = calendar.getQueryStringURL();
							
							document.write( "<a href=\"" + queryStringURL + "\"><span class=\"month\">" + calendar.monthName + "</span> <span class=\"year\">" + calendar2.year + "</span></a>" );
							</script>
						</h4>
						<a class="next_arrow" href="javascript:void(0)" onClick="calendar.changeMonth('nextMonth', $('#program').val(), $('#venue').val())"><img src="<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg" alt="right arrow;" onMouseOver="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow_hover.jpg'" onMouseOut="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg'" width="19" height="19" /></a>
					</div>
					<!-- end CALENDAR NAME & ARROW -->
					
					<!-- start CALENDAR TABLE -->
					
					<table class="calendar" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">Sun</th>
								<th scope="col">Mon</th>
								<th scope="col">Tue</th>
								<th scope="col">Wed</th>
								<th scope="col">Thu</th>
								<th scope="col">Fri</th>
								<th scope="col">Sat</th>
							</tr>
							<tr class="table-border">
								<td colspan="7"></td>
							</tr>
						</thead>
						<tbody>
							<script>
							document.write( calendar.tableCellsHTML );
							</script>
						</tbody>
					</table>
					<!-- start CALENDAR TABLE -->
					
				</div>
				<!-- end MINI CALENDAR  -->
				
				
				
				
				<!-- start MINI CALENDAR 2 -->
				<div id="cal2" class="calendar-mini">
				
					<!-- start CALENDAR NAME & ARROW -->
					<div class="month-name">
						<a class="previous_arrow" href="#" style="visibility:hidden;"><img src="<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg" alt="right arrow;" onClick="calendar.changeMonth('previousMonth', 'cal2')" onMouseOver="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow_hover.jpg'" onMouseOut="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg'" width="19" height="19" /></a>
						<h4 class="month-name-text">
							<script>
							
							/* generate a query string, for "month" label hyperlink */
							queryStringURL = calendar2.getQueryStringURL();
							
							document.write( "<a href=\"" + queryStringURL + "\"><span class=\"month\">" + calendar2.monthName + "</span> <span class=\"year\">" + calendar2.year + "</span></a>" );
							</script>
						</h4>
						<a class="next_arrow" href="#" style="visibility:hidden;" onClick="calendar.changeMonth('nextMonth', 'cal2')"><img src="<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg" alt="right arrow;" onMouseOver="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow_hover.jpg'" onMouseOut="this.src='<?php echo get_template_directory_uri() ?>/assets/events/images/widget_calendar_right_arrow.jpg'" width="19" height="19" /></a>
					</div>
					<!-- end CALENDAR NAME & ARROW -->
					
					<!-- start CALENDAR TABLE -->
					<table class="calendar" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">Sun</th>
								<th scope="col">Mon</th>
								<th scope="col">Tue</th>
								<th scope="col">Wed</th>
								<th scope="col">Thu</th>
								<th scope="col">Fri</th>
								<th scope="col">Sat</th>
							</tr>
							<tr class="table-border">
								<td colspan="7"></td>
							</tr>
						</thead>
						<tbody>
							<script>
							document.write( calendar2.tableCellsHTML );
							</script>
						</tbody>
					</table>
					<!-- start CALENDAR TABLE -->
					
				</div>
				<!-- end MINI CALENDAR 2  -->
				
				
				</div><!-- /.feed -->
				
			</div>
			<!-- end right column -->
			
			
					
		
		</div>
	</div><!-- /.row -->
  

  </div>
</div>

<?php get_footer(); ?>