<?php
/**
 * The Template for displaying all single posts.
 */
get_header(); ?>

<!-- colorbox lightbox -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/colorbox/example2/colorbox.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/colorbox/jquery.colorbox.js"></script>
<!-- end colorbox lightbox -->

<div class="container top-container">

	<div class="row">
            
        <?php
        // get category of page/post
        if ( ! in_category( 'full-width', $post->ID ) && ! in_category( 'fullwidth', $post->ID ) ) {
            
            
            // set width of container
            $col_width = "col-sm-9";
            
            ?>
            
		<div class="col-sm-3" id="left-sidebar">
			
			<?php if ( is_active_sidebar( 'sidebar-news-page' ) ) : ?>
			<div id="first" class="widget-area" role="complementary">		
				<?php dynamic_sidebar( 'sidebar-news-page' ); ?>
			</div>
			<?php endif; ?>
			
		</div>	
        <?php } ?>
	

		<div class="<?php echo $col_width; ?>">
			
			<?php echo get_breadcrumb('post'); ?>
		
			<h2>
                            <span class="social_links pull-right" >
				<?php echo social_links( $post->ID ); ?>
                            </span>
                            <span id="post_title"><?php print get_the_title($post->ID); ?></span>
                        </h2>
			

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
			
		
		
	</div>
	
	
	
	
</div>
<?php get_footer(); ?>