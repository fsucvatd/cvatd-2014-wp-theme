<?php
/**
 * Template for displaying all text pages.
 */
get_header(); ?>


<!-- colorbox lightbox -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/colorbox/example2/colorbox.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/colorbox/jquery.colorbox.js"></script>
<!-- end colorbox lightbox -->

<div class="container top-container">




	<div class="row">
		<div class="col-sm-3">
			<?php print_subnav($post); ?>
		</div>
	

		<div class="col-sm-9">
			
			<?php echo get_breadcrumb('page'); ?>
		
			<h2><?php print get_the_title($post->ID); ?></h2>
			
			

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
		
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
  

  </div>
</div>


<?php get_footer(); ?>