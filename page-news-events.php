<?php 
/**
   Template Name: News Page
 * The Template for displaying the news page.
 */
 get_header(); ?>

	
<div class="container top-container"><!-- news & events -->

	<div class="row">
	
		<div class="col-sm-8"><!-- news -->
		
			<div class="feed">
		
				<div class="row feed-col-header">
					<h4><?php bloginfo('name'); ?> News
					<span class="social_links pull-right" >
						<?php echo social_links(); ?>
					</span>
					</h4>
				</div>				
				
				<?php display_post_feed($post,5); ?>
				
			</div><!-- /.feed -->
		</div>	
				
				
				
				
		<div class="col-sm-4">
			<div class="feed">
				<div class="row feed-col-header">
					<h4>Upcoming Events</h4>
				</div>
				
					
				<?php if ( is_active_sidebar( 'sidebar-right-1' ) ) : ?>
				<div class="sidebar">
					<div id="first" class="container widget-area" role="complementary">
						<div class="row">
						<?php dynamic_sidebar( 'sidebar-right-1' ); ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			
			</div>
		</div>
	</div><!-- /.row -->
	
	
	
	

</div><!-- /.container -->

<?php get_footer(); ?>