<?php get_header(); ?>


<?php

// get last three posts from featured category
$num_featured_posts = 0;
$featured_posts = NULL;

// get "featured home" category ids
$category_ids = array();
$category_ids[] = get_category_id("featured home");
$category_ids[] = get_category_id("featured");
$category_ids = implode(",", $category_ids);


// create args for get_posts()
$args = array(
        //'category'         => 'news,-16',
        'category'         => $category_ids,
        'orderby'          => 'post_date',
        'post_status'      => 'publish',
        'posts_per_page'   => '3'
);
$postslist = get_posts( $args );
if ($postslist) {?>

    <div id="front-page-carousel" class="carousel slide"><!-- carousel -->
            <div class="carousel-inner"><!-- wrapper for slides -->
                
        <?php
        $featured_posts = array();

        // loop through posts
        foreach($postslist as $post_) {
            $num_featured_posts++;
            
            // set "active" class style for first slide
            if ($num_featured_posts == 1) $display = " active";
            else $display = "";
            
            // if College Video, change "Continue Reading" button to "Play Video"
            if ($post_->post_title == "College of Visual Arts, Theatre & Dance Video") {
                $buttonText = "Play Video";
                $lightbox = " youtube cboxElement\" rel=\"lightbox";
                //$link = "http://new.cvatd.fsu.edu/wp-content/uploads/2014/03/FSU_Your_Moment_Trailer.mp4";
                $link = get_permalink($post_);
            } else {
                $buttonText = "Continue Reading";
                $lightbox = "";
                $link = get_permalink($post_);
            }
            
        ?>

		<div class="item <?php echo $display; ?>">
			<img src="<?php echo display_featured_post_img($post_); ?>" alt="" />
			<div class="carousel-caption">
				<div class="cc-title"><?php echo $post_->post_title; ?></div>
				<p class="cc-text"><?php echo $post_->post_excerpt; ?></p>
				<p><a href="<?php echo $link ?>" class="btn btn-carousel btn-sm youtube cboxElement" rel="lightbox" role="button"><?php echo $buttonText; ?> &raquo;</a></p>
			</div>
		</div>

        <?php } // end foreach($postslist as $post_)
        ?>

	</div><!-- /.carousel-inner -->
	
	<!-- controls -->
	<a class="carousel-control left" href="#front-page-carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="carousel-control right" href="#front-page-carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span></a>
	
</div><!-- /.carousel -->

   
	

<script>
// start the carousel
var $j = jQuery.noConflict();
$j(document).ready(function(){
	$j('#front-page-carousel').carousel({
		interval: 6000
	});
});

//lightbox for video
$(document).ready(function() {
  $("a[rel=lightbox]").click(
    function(event) {
      event.preventDefault();
      var elementURL = $(this).attr("href");
      $.colorbox({iframe: true, href: elementURL, innerWidth: 645, innerHeight: 509});
      });
  });
</script>

<?php } // end if ($postslist)
?>


<div class="container"><!-- news & events -->

	<div class="row">
	
		<div class="col-sm-8"><!-- news -->
		
			<div class="feed">
		
				<div class="row feed-col-header">
					<h4><?php bloginfo('name'); // Denise, example of making universal ?> News
					<span class="social_links pull-right" >
						<?php echo social_links(); ?>
					</span>
					</h4>
				</div>			
				
				<?php display_post_feed(null,5); ?>
				
			</div><!-- /.feed -->
		</div>					
				
				
				
				
			<?php
	
			/*
			// get posts
			// http://codex.wordpress.org/Template_Tags/get_posts
			$args = array(
				'posts_per_page'   => 5,
				'category'         => 'news,-16',
				
				//'category'         => $categories,
				
				'orderby'          => 'post_date',
				'post_status'      => 'publish',
				'exclude'          => '16',
				);
			$posts = get_posts($args);
			if ($posts) {
				foreach($posts as $post) {
					// setup global post data
					setup_postdata($post);
					
				?>
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<?php echo display_post_img($post); ?>
					</div>
					<div class="col-sm-6 col-md-8">
						
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<div class="feed-date-tags"><?php 
							the_time('F j, Y');
							
							$post_categories = wp_get_post_categories($post->ID);
							if (count($post_categories) > 0) print ' - ';
							foreach($post_categories as $c){
								$cat = get_category( $c );
								if ($cat->slug != 'uncategorized'){  ?>
								<a class="btn btn-xs btn-tag" href="<?php print $cat->slug ?>" rel="bookmark" title="More posts in <?php print $cat->name; ?>"><?php print strtolower($cat->name); ?></a>
								
							<?php
								}
							}
						
						?></div>
						<?php
						
						if ($post->post_excerpt) {
							// if there is an excerpt then display it
							$htmlout = shorten_post_content($post->post_excerpt,180);
						} else {
							// otherwise use content but remove images
							$htmlout = shorten_post_content($post->post_content,180);
						}
						
						echo $htmlout;
						?>
						... <a href="<?php the_permalink(); ?>">Continue reading &raquo;</a>
					</div>
				</div><!-- /.row -->
				<?php
				}
			} else { 
				print "No posts found."; 
			}
			?>
					
			
				<div class="row feed-col-footer">
					<!--<a href="news" title="More news">More news &raquo;</a>-->
					<?php echo om_pagination(); ?>
				</div>
				
			</div><!-- /.feed -->
		</div>
		*/
		?>
	
	
	
		<div class="col-sm-4">
			<div class="feed">
				<div class="row feed-col-header">
					<h4>Upcoming Events</h4>
				</div>
				
					
				<?php if ( is_active_sidebar( 'sidebar-right-1' ) ) : ?>
				<div class="sidebar">
					<div id="first" class="container widget-area" role="complementary">
						<div class="row">
						<?php dynamic_sidebar( 'sidebar-right-1' ); ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			
			</div>
		</div>
	</div><!-- /.row -->
	
	
	
	

</div><!-- /.container -->

<?php get_footer(); ?>