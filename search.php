<?php
/**
 * Template for Search results page
 */
get_header(); ?>

<div class="container top-container">




	<div class="row">
		<div class="col-sm-3">
			
			<?php if ( is_active_sidebar( 'sidebar-news-page' ) ) : ?>
			<div id="first" class="widget-area" role="complementary">		
				<?php dynamic_sidebar( 'sidebar-news-page' ); ?>
			</div>
			<?php endif; ?>
			
		</div>
		<div class="col-sm-9">
		
		
			<div class="crumbs"><a href="/">Home</a> <span class="arrow">&raquo;</span> Search</div>
		
		<?php if ( have_posts() && strlen( trim(get_search_query()) ) != 0 ) { ?>
		
			<h2>Search Results for "<span class="search_term"><?php echo get_search_query(); ?></span>"</h2>
			
			
			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php /*if ( has_post_thumbnail() ) { ?>
					
						<a class="thumbnail" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>" >
							<?php the_post_thumbnail( 'medium' ); ?>
						</a>
				 <?php } */?>  
					
				<h3><a title="<?php the_title();?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php 
				
				
				//if ($post->post_excerpt) {
					$exc = get_the_excerpt();  
					print strip_shortcodes($exc);
					print ' ... <a href="'. get_permalink($post->ID) .'">Continue reading &raquo;</a>';
				//}
				?>
				
			
			   
			<?php endwhile; ?>
			
			<ul class="pager">
			<li><?php next_posts_link('<i class="icon-chevron-left"></i>&nbsp; Older Results') ?></li>
			<li><?php previous_posts_link('Newer Results &nbsp;<i class="icon-chevron-right"></i>') ?></li>
			</ul>
			
		<?php } else { // nothing found ?>
			
			<h2 class="page-title">No results Found</h2>
			<p>
				It seems we can't find what you're looking for.
				Please try again with a different search term.
			</p>
			<div class="well">
				<?php get_search_form(); ?>
			</div><!--/.well -->
			
		<?php } ?>
		<?php //paging_content_nav( 'nav-below' ); ?>
		
			
        </div>
    </div>
	
	
	
	
</div>

<?php get_footer(); ?>