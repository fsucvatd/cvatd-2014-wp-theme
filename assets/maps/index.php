<?php
	include get_template_directory() . "/assets/maps/getfacilities.php";
?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/assets/maps/style.css'; ?>">
	
	<!-- include Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	
	
    <script>
		// The following example creates complex markers to indicate beaches near
		// Sydney, NSW, Australia. Note that the anchor is set to
		// (0,32) to correspond to the base of the flagpole.
		
		function initialize() {
			
			if(new_center == null)
				new_center = new google.maps.LatLng(-91.9240850749, 47.9119132072);
			
		  var mapOptions = {
			zoom: 17,
			center: new_center,
			mapTypeId: google.maps.MapTypeId.MAP
		  }
		  map = new google.maps.Map(document.getElementById('map-canvas'),
										mapOptions);
		  setMarkers(map, facilities, markers);
		}
		
		/**
		 * Data for the markers consisting of a name, a LatLng and a zIndex for
		 * the order in which these markers should display on top of each
		 * other.
		 */
		var new_center = null;
		var markersHidden = new Array();
		var map;
		var markers = new Array();
		var facilities = <?php echo json_encode($facilities); ?>;
		
		function setMarkers(map, locations) {
		  // Add markers to the map
		
		  // Marker sizes are expressed as a Size of X,Y
		  // where the origin of the image (0,0) is located
		  // in the top left of the image.
		
		  // Origins, anchor positions and coordinates of the marker
		  // increase in the X direction to the right and in
		  // the Y direction down.  
		  
			var infowindow = new google.maps.InfoWindow({
				content: ''
			});
			
			for (var i in locations) {
				
				var isVisible = true;
				
				// hide visibility of marker, if its index added to markersHidden array
				if (markersHidden.indexOf(i) > -1)
					isVisible = false;
				
				var place = locations[i];
				var contentString = locations[i].description;
				var myLatLng = new google.maps.LatLng(place.latitude, place.longitude);
				var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: place.name,
					zIndex: place.id,
					visible: isVisible
				});
			
				var contentStr = '<div id="content">'+
					  '<div id="siteNotice">'+
					  '</div>'+
					  getImage(place.imageurls, place.name) +
					  '<h3 id="firstHeading" class="firstHeading">' + place.name + itemExists(place.abbreviation, "abbreviation") + '</h3>'+
					  '<div id="bodyContent">'+
					  '<p style="margin:0">' + place.address + '<br />' +
					  //itemExists(place.website, "website") +
					  itemExists(place.phone, "phone") +
					  itemExists(place.type, "type") +
					  itemExists(place.program, "program(s)") +
					  '<a href=\"https://maps.google.com/maps?saddr=' + place.address + '\" target=\"_blank\" class="getdirections"><img class=\"mapsdirections\" src=\"http://new.cvatd.fsu.edu/wp-content/uploads/2013/11/download.jpeg\" alt=\"Google Maps: Directions\" /></a><a href=\"https://maps.google.com/maps?saddr=' + place.address + '\" target=\"_blank\">Get Directions</a>' +
					  '</p>' +
					  '<p class="clear-left">' + contentString + itemExists(place.website, "website") + '</p>' +
					  '</div>'+
					  '</div>';
				
				// set infowindow trigger events
				bindInfoWindow(marker, map, infowindow, contentStr);
				markers.push(marker);
				setLink(marker, i);
			}
		}
		
		// assigns marker object to text link in navigation
		function setLink(marker, i) {
		
			var link = document.getElementById("link" + i);
		
			link.onclick = function(){
				google.maps.event.trigger(marker, 'click');
			}
		}
		
		function bindInfoWindow(marker, map, infowindow, strDescription) {
			
			//if window open, close
			if (infowindow)
				infowindow.close();
			
			// on mouseover marker, open infowindow
			google.maps.event.addListener(marker, 'mouseover', function() {
				infowindow.setContent(strDescription);
				infowindow.open(map, marker);
			});
			
			// on click marker, zoon in and open info window
			google.maps.event.addListener(marker, 'click', function() {
				map.setZoom(18);
				map.setCenter(marker.getPosition());
				infowindow.setContent(strDescription);
				infowindow.open(map, marker);
			});
			
			// on infowindow close, zoom out
			google.maps.event.addListener(infowindow,'closeclick',function(){
				map.setZoom(5);
			});
		}
		
		function itemExists(str, type) {
			
			var str_ = "";
			
			if (str == "" || str == null)
				str_ = "";
			else if (type == "abbreviation")
				str_ = " (" + str + ")";
			else if (type == "website")
				str_ = " <a href=\"" + str + "\" target=\"_blank\" title=\"" + str + "\">Learn More &raquo;</a><br />";
			else if (type == "phone")
				str_ = str + "<br />";
			else
				str_ = type.capitalize() + ": <span class=\"tag\">" + str + "</span><br />";
				
			return str_;
		}
		
		function getImage(images_, name) {
			
			var imgcode = "";
			var imageurl;
			
			if (images_ != "") {
				imgurl = images_.toString().split(",");
				imgcode = "<img src=\"" + imgurl[0] + "\" alt=\"" + name + "\" />";
			}
			
			return imgcode;
		}
		
		// parse string, according to its type
		function parseString(str, type) {
			
			var str_ = "";
			
			// if string is an address, parse string delimited by commas
			if (type == "address") {
				str = str.split("; ");
				
				for (var a in str)
					str_ = str_ + str[a] + "<br />";
			}
			
			return str_;
		}
		
		String.prototype.capitalize = function() {
			return this.charAt(0).toUpperCase() + this.slice(1);
		}
		
		
		function getPrograms() {
			
			var programs = new Array();
			var programsUnique = new Array();
			
			//add all/default facilities
			programs.push("All CVATD Programs");
			
			// get all programs for each facility
			for (var a in facilities) {
				
				// check if value contains commas aka multi values
				if (facilities[a].program.indexOf(", ") > -1) {
				
					// separate program (strings values) from commas
					var tempPrograms = facilities[a].program.split(", ");					
												
					// add separated values to main programs array
					for (var b in tempPrograms) {
						
						//check if main program array already contains this value
						//don't add duplicate values to array
						if (programs.indexOf(tempPrograms[b]) == -1) {
							
							//trim leading and trailing white space
							var tempProgram = tempPrograms[b].replace(/^\s+|\s+$/g, "");
							
							programs.push(tempProgram);
							
						}
					}
				}
				
				else {
					//trim leading and trailing white space
					var tempProgram = facilities[a].program.replace(/^\s+|\s+$/g, "");
					
					//check if main program array already contains this value
					//don't add duplicate values to array
					if (programs.indexOf(tempProgram) == -1) {
						program = programs.push(tempProgram);
					}
				}
			}
			
			//alphabetize array
			programs.sort();
						
			return programs;
		}
		
		function getProgram() {
			var program = '<?php if (isset($_GET['program'])) echo $_GET['program']; else echo ""; ?>';
			return program;
		}
		
		/* called from html body, facilities for loop JS */
		/* determines if the facilities.type contains the program (passed from url get 
			variable program) */
		function containsProgram(programs_) {
			
			var contains = false;
			var programs_arr = new Array();
			
			// get url get variable program
			var program = getProgram();
			
			// parse programs_ into array; delimeter is a comma and space (, )
			programs_arr = programs_.split(",");
			
			// go through new programs_arr
			for (b in programs_arr) {
				// remove leading and trailing whitespace characters
				var program_temp = programs_arr[b].trim();
				
			// determine if facility type contains program variable value
				if ( program_temp.toLowerCase() == program.toLowerCase() ) {
					contains = true;
					break;
				}
			}
			
			return contains;
			
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);

  
    </script>
	
  	
	
	<div class="col-sm-4">
		<div class="row">
		
		<div id="navigation" style="width:100%">
			
			<p>View Facilities by Program: </p>
			
			<form action="" method="get" name="facilities">
			
				<select id="programs_dropdown" name="programs_dropdown">
					<script>
					var selected = "";	// marks whether item in list will be selected by default
					
					// get all programs from facilities array
					var programs = getPrograms();
					
					//show all programs
					for (var a in programs) {
						
						//get url variable; if value is listed in facilities dropdown, make value selected in drowndown
						if ( getProgram().capitalize().indexOf( programs[a] ) > -1 ) {
							selected = "selected";
						}
						
						// display facility value as option in dropdown
						document.writeln("<option class=\"programslist\" value=\"" + programs[a] + "\" " + selected + ">" + programs[a] + "</option>");
						
						selected = "";
					}
					</script>
				</select>
			</form>
			
			<h4 style="margin-top:20px;">
			<script>
	var $j = jQuery.noConflict();
			//get value from programs dropdown
			var selectedProgram = $j("#programs_dropdown").val();
			
			if (selectedProgram.indexOf("All") > -1)
				selectedProgram = "All CVATD";
			
			document.write(selectedProgram);
			</script>
			 Facilities
			</h4>
			
	
			<!-- display all facilities -->
			<script>
			
				var containsSarasotaFacilities = false;
				var containsTallahasseeFacilities = false;
				var otherlocations = false;
				var numFacilities = facilities.length - 1;
			
				/* update initial values, when focused or blurred */
				$j(document.body).on('change', '#programs_dropdown' ,function(){
					this.defaultValue = $j(this).val();
					window.location = './?program=' + this.value;
				});
				
				// display Tallahassee facilities
				document.write("<h5>Tallahassee</h5>");
				
				for (var a in facilities) {				
					
					var hideLink = "";
					
					// if facility programs does not contain get variable programs value, or
					// the program get variable is not "ALL" programs, hide facility
					if (containsProgram(facilities[a].program) == false && 
						getProgram().indexOf("All") < 0 && 
						getProgram() != ""
					) {
						hideLink = " style=\"display:none;\"";
						markersHidden.push(a);
					}
					else {
					
						// update map center to reflect selected program's facilities
						if ( new_center === null )
							new_center = new google.maps.LatLng(facilities[a].latitude, facilities[a].longitude);
					
						// dont display non-tallahassee facilities
						if ( facilities[a].address.toLowerCase().indexOf("tallahassee") > -1 ) {
							containsTallahasseeFacilities = true;
						}
						else if ( facilities[a].address.toLowerCase().indexOf("sarasota") > -1 ) {
							containsSarasotaFacilities = true;
							continue;
						}
						else {
							otherlocations = true;
							continue;
						}
						
					}
					
					
					
					// if no facilities in tallahassee, inform user
					if ( containsTallahasseeFacilities == false && (a == numFacilities ))
					document.writeln("<p style=\"margin-left:15px; color: #aaa;\">There are no Tallahassee facilities for this program.</p>");
					
					
					// display link of facility text
					document.writeln("<a id=\"link" + a + "\" href=\"#\"" + hideLink + "><p class=\"facility\">" + facilities[a].name + "</p></a>");
				}
				
				
				
				// display Sarasota facilities
				if (containsSarasotaFacilities)
					document.write("<h5>Sarasota</h5>");
				for (var a in facilities) {
					
					if (  facilities[a].address.toLowerCase().indexOf("sarasota") < 0 )
						continue;
					
					var hideLink = "";
					
					// if facility programs does not contain get variable programs value, or
					// the program get variable is not "ALL" programs, hide facility
					if (containsProgram(facilities[a].program) == false && 
						getProgram().indexOf("All") < 0 && 
						getProgram() != ""
					) {
						hideLink = " style=\"display:none;\"";
						markersHidden.push(a);
					}
					else
					
						// update map center to reflect selected program's facilities
						if ( new_center === null )
							new_center = new google.maps.LatLng(facilities[a].latitude, facilities[a].longitude);
						
					
					document.writeln("<a id=\"link" + a + "\" href=\"#\"" + hideLink + "><p class=\"facility\">" + facilities[a].name + "</p></a>");
				}
				
			</script>
		</div>
		</div>
	</div>
	<div class="col-sm-8">
		
	    <div id="map-canvas" style="width:100%"></div>
	</div>