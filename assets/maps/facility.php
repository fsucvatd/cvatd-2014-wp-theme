<?php
class Facility {
	
	private $id,
			$name,
			$abbreviation,
			$shortname,
			$description,
			$address,
			$website,
			$latitude,
			$longitude,
			$imageurls,
			$type,
			$program,
			$phone,
			$contact,
			$mailcode;
			
	function setProperty($property, $value) {

		$status = TRUE;
		
		if ($property == "id")
			$this->id = $value;
		else if ($property == "name")
			$this->name = $value;
		else if ($property == "abbreviation")
			$this->abbreviation = $value;
		else if ($property == "shortname")
			$this->shortname = $value;
		else if ($property == "description")
			$this->description = $value;
		else if ($property == "address")
			$this->address = $value;
		else if ($property == "website")
			$this->website = $value;
		else if ($property == "latitude")
			$this->latitude = $value;
		else if ($property == "longitude")
			$this->longitude = $value;
		else if ($property == "imageurls")
			$this->imageurls = explode(", ", $value);
		else if ($property == "type")
			$this->type = $value;
		else if ($property == "program")
			$this->program = $value;
		else if ($property == "phone")
			$this->phone = $value;
		else if ($property == "contact")
			$this->contact = $value;
		else if ($property == "mailcode")
			$this->mailcode = $value;
		else
			$status = FALSE;
			
		return $status;
	}
	
	function getProperty($property) {
		
		$value = "";
		
		if ($property == "id")
			$value = $this->id;
		else if ($property == "name")
			$value = $this->name;
		else if ($property == "abbreviation")
			$value = $this->abbreviation;
		else if ($property == "shortname")
			$value = $this->shortname;
		else if ($property == "description")
			$value = $this->description;
		else if ($property == "address")
			$value = $this->address;
		else if ($property == "website")
			$value = $this->website;
		else if ($property == "latitude")
			$value = $this->latitude;
		else if ($property == "longitude")
			$value = $this->longitude;
		else if ($property == "imageurls")
			$value = $this->imageurls;
		else if ($property == "type")
			$value = $this->type;
		else if ($property == "program")
			$value = $this->program;
		else if ($property == "phone")
			$value = $this->phone;
		else if ($property == "contact")
			$value = $this->contact;
		else if ($property == "mailcode")
			$value = $this->mailcode;
		else
			$value = FALSE;
			
		return $value;
	}
	
}
?>