<?php

class GoogleDoc {

	private $fileurl;
	
	function __construct($url) {
       $this->setFileURL($url);
	}
	
	function setFileURL($value) {
		$this->fileurl = $value;
	}
	
	function openFile() {
		
		$spreadsheet_data;
		$spreadsheet_url = $this->fileurl;
		
		if(!ini_set('default_socket_timeout', 15))
			$spreadsheet_data = FALSE;
			
		if ( $spreadsheet_data !== FALSE && (($handle = fopen($spreadsheet_url, "r")) !== FALSE) ) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
				$spreadsheet_data[]=$data;
			
			fclose($handle);
			
		} else {
			//die("Problem reading csv");
			$spreadsheet_data = FALSE;
		}
		
		return $spreadsheet_data;
	}		
	
}

?>