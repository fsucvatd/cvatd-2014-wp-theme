function Calendar(func, month, day, year, programSelected, venueSelected, monthName) {
	
	this.monthNumber;
	this.monthName;
	this.day;
	this.dayView = false;
	this.year;
	this.program = "";
	this.venue = "";
	this.tableCellsHTML;
	this.tableCellsHTML_next;
	this.eventList;
	this.eventListToday;
	this.functionURL = themesUrl + "calendar-functions.php";
	
	// set program
	if ( programSelected )
		this.program = programSelected;
		
	// set venue
	if ( venueSelected )
		this.venue = venueSelected;
	
	
	// create new Date object
	this.curDate = new Date();
	this.curDate.setMonth( month - 1 );
	this.curDate.setYear( year );
	
	if ( func == "dayView" ) {
		this.curDate.setDate( day );
		this.dayView = true;
	}
	else
		this.curDate.setDate(1);
	
	if ( func == "getNextMonth" ) {
		this.curDate.setDate( -1 );
		this.curDate.setMonth( this.curDate.getMonth() + 2 );
	}
	
	// set monthname, if value passed
	if ( monthName && monthName != "" ) 
		this.monthName = monthName;
	
	this.resetDate(this.curDate);

}

	Calendar.prototype.resetDate = function( curDate_ ) {
		
		this.curDate = curDate_;
		this.monthNumber = curDate_.getMonth();
		this.monthName = this.getMonthName();
		this.day = curDate_.getDate();
		this.year = curDate_.getFullYear();	
		
		var html = "";
		var daynum = null;
		
		
		// check if day number has been given
		if ( this.dayView )
			daynum = this.day;

		// update table cells for 1st calendar
		$.ajax({
			url: this.functionURL,
			type: "POST",
			async: false,
			data: { month: this.monthName, year: this.year, content: "CalendarTableCells", program: this.program, venue: this.venue, day: daynum }
		})
			.done(function( msg ) {
				html = msg;
			});
			
			this.tableCellsHTML = html;
			
		
		// update table cells for 2nd calendar
		$.ajax({
			url: this.functionURL,
			type: "POST",
			async: false,
			data: { month: this.monthName, year: this.year, content: "CalendarTableCells_next", program: this.program, venue: this.venue }
		})
			.done(function( msg ) {
				html = msg;
			});
			
			this.tableCellsHTML_next = html;
		
		
		// set event list html
		$.ajax({
			url: this.functionURL,
			type: "POST",
			async: false,
			data: { month: this.monthName, year: this.year, content: "calendarlist", direction: "", day: daynum, program: this.program, venue: this.venue }
		})
			.done(function( msg ) {
				html = msg;
			});
			
		this.eventList = html;
                
                
		
		// set today's event list html
		$.ajax({
			url: this.functionURL,
			type: "POST",
			async: false,
			data: { month: "", year: "", content: "calendarlist", direction: "", day: "", program: this.program, venue: this.venue, isToday: true }
		})
			.done(function( msg ) {
				html = msg;
			});
			
		this.eventListToday = html;

	}

	Calendar.prototype.getMonthName = function (monthNumber_) {
		
		
		var month = new Array();
		var monthNumber = this.monthNumber;
		
		if (monthNumber_)
			monthNumber = monthNumber_;
		
		month[0] = "January";
		month[1] = "February";
		month[2] = "March";
		month[3] = "April";
		month[4] = "May";
		month[5] = "June";
		month[6] = "July";
		month[7] = "August";
		month[8] = "September";
		month[9] = "October";
		month[10] = "November";
		month[11] = "December";
		
		if (this.monthName)
			return this.monthName;
		else
			return month[monthNumber];
	}

	Calendar.prototype.changeMonth = function (direction, program, venue) {
		
		if (!venue || venue == null)
			venue = "";
			
		//update html table for calenar 1
		this.updateHTML( "#cal1", direction, program, venue );
		
		//update html table for calenar 2
		this.updateHTML( "#cal2", direction, program, venue );
		
		
		// get current date from html
		var month = $( "#cal1 .month" ).text();
		var year = $( "#cal1 .year" ).text();
				
		// update event list
		$( '.left66').load(this.functionURL, { "month": month, "year": year, "content": "calendarlist", "direction": direction, "program": program, "venue": venue });

	}
	
	
	Calendar.prototype.changeProgram = function (program, venue) {
		
		if (!venue || venue == null)
			venue = "";
		
		//update html table for calenar 1
		this.updateHTML( "#cal1", "", program, venue );
		
		//update html table for calenar 2
		this.updateHTML( "#cal2", "", program, venue );
		
		// get current date from html
		var month = $( "#cal1 .month" ).text();
		var year = $( "#cal1 .year" ).text();
				
		// update event list
		$( '.left66').load(this.functionURL, { "month": month, "year": year, "content": "calendarlist", "direction": "", "program": program, "venue": venue });
	}
	
	Calendar.prototype.updateHTML = function ( div, direction, program, venue ) {
		
		// get current date from html
		var month = $( div + " .month" ).text();
		var year = $( div + " .year" ).text();
		
		
		// update calendar header title, with new month and year
			$( div + ' .month-name-text').load(this.functionURL, { "month": month, "year": year, "content": "calendarHeader", "direction": direction });
		
		// update calendar table cells
		$( div + ' .calendar tbody').load(this.functionURL, { "month": month, "year": year, "content": "CalendarTableCells", "direction": direction, "program": program, "venue": venue });
	}
	
	Calendar.prototype.getQueryStringURL = function () {
		
		/* generate a query string, for "month" label hyperlink */
		var monthString = ""; // place holder for month string in url query
		var yearString = ""; // place holder for year string in url query
		var venueString = ""; // place holder for venue string in url query
		var locationString = ""; // place holder for location string in url query
		var programString = "";
		var queryStringURL = "";	// string to combine all GET variables values into a URL
		
		if ( this.monthNumber != "" && this.monthNumber != null )
			monthString = ";month=" + (this.monthNumber+1);
		if ( this.year != "" && this.year != null )
			yearString = ";year=" + this.year;
		if ( this.venue != "" && this.venue != null )
			venueString = ";venue=" + this.venue;
		if ( this.program != "" && this.program != null )
			programString = ";program=" + this.program;
			
		queryStringURL = "./?views=" + monthString + yearString + venueString + programString;	
		
		return queryStringURL;
	}