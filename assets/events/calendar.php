<?php


//include events
include "events.php";
include "parsing_functions.php";

function changeCalendarHeader( $month, $year, $direction, $program = NULL ) {
	
	// get next month
	if ( $direction == "nextMonth" ) {
		$newmonth = getNextMonth( $month, $year );
		$newyear = getNextYear( $month, $year );
	}
	
	// get previous month
	else if ( $direction == "previousMonth" ) {
		$newmonth = getLastMonth( $month, $year );
		$newyear = getLastYear( $month, $year );
	}
	
	else {
		$newmonth = $month;
		$newyear = $year;
	}
	
	$_getURLqueryString = getURLQueryString( "", $program, $newmonth, "", $newyear );
	
	echo "<a href=\"$_getURLqueryString\"><span class=\"month\">$newmonth</span> <span class=\"year\">$newyear</span></a>";
}


function updateViewTitle( $month, $year, $direction ) {
	
	// get next month
	if ( $direction == "nextMonth" ) {
		$newmonth = getNextMonth( $month, $year );
		$newyear = getNextYear( $month, $year );
	}
	
	// get previous month
	if ( $direction == "previousMonth" ) {
		$newmonth = getLastMonth( $month, $year );
		$newyear = getLastYear( $month, $year );
	}
}

function getNextMonth($month, $year) {
	
	if ( $month == "January" || $month == "january" )
		$nextMonth = "February";
	
	else
		$nextMonth = date("F", strtotime("$month 28 + 5 days" ));
		
	return $nextMonth;
}

function getNextYear($month, $year) {

	if ($month == "December")
		$nextYear = $year + 1;
	else
		$nextYear = $year;
		
	return $nextYear;
}


function getLastMonth($month) {
	
	$lastMonth = date("F", strtotime("$month 1 - 1 day"));
	return $lastMonth;
}

function getLastYear($month, $year) {

	if ($month == "January")
		$lastYear = date("Y", strtotime("$month 1, $year - 30 days"));
	else
		$lastYear = $year;	
		
	return $lastYear;
}

function displayCalendarMonth( $month, $year, $direction, $program, $venue, $day ) {
	
	$html = "";
	
	//update month and year, if user clicked next or previous arrow buttons for month
	if ( $direction == "nextMonth" ) {
		$month = getNextMonth( $month, $year );
		$year = getNextYear( $month, $year );
	}
	else if ( $direction == "previousMonth" ) {
		$year = getLastYear( $month, $year );
		$month = getLastMonth( $month, $year );
	}
	
	
	// get number of days in month
	$days_in_month = cal_days_in_month(CAL_GREGORIAN, date("n", strtotime($month)), date("Y", strtotime($year)));
	
	// get today's date
	$today = date('F j Y');
	
	$first_date = date('w', strtotime('first day of $month $year'));	//first numerical date of the month
	$DAYS_IN_WEEK = 7;		// constant; number of days in a week
	$num_weeks = getWeeks($year, $month); //number of weeks in current month
	
	//get 1st day_name of month; day month starts on
	$first_day_name = date('w', strtotime("$month $day_placeholder $year"));
	
	// print each week of month
	for ($cur_week = 0, $day_placeholder = 1 - $first_day_name, $cur_day = $first_date; $cur_week < $num_weeks; $cur_week++) {
		
		$html .= "<tr>";
		
		// print each day of week
		for ( $day_of_week = 0; $day_of_week < 7; $day_of_week++, $day_placeholder++, $cur_day++ ) {
			
			// check if a specific day is given or $day_placeholder is today's day, if so set cell color
			$cellClass = "";
			$curDate = date("F j Y", strtotime($month . " " . $day_placeholder . " " . $year));
			if ( $day != NULL ) {
				
				$givenDate = date("F j Y", strtotime($month . " " . $day . " " . $year));
				if ( $curDate == $givenDate )
					$cellClass = "bgRed ";
			}
			else if ( $today == $curDate ) {
				$cellClass = "bgRed ";
			}
			
			// change color of cell based on number of events
			// get number of events
			$numEvents = getNumberEvents( $curDate, $program, $venue );
			$cellClass2 = "";
			if ( $numEvents >= 4 && $cellClass == "" )
				$cellClass2 = "bgDarkGrey ";
			else if ( $numEvents >= 2 && $cellClass == "" )
				$cellClass2 = "bgMediumGrey ";
			else if ( $numEvents >= 1 && $cellClass == "" )
				$cellClass2 = "bgLightGrey ";
			
			// generate new url, using current GET variables along with the current day in the calendar cell
			$_GETstring = getURLQueryString( $venue, $program, $month, $day_placeholder, $year );				
			
			// display cells with day numbers
			if ( $day_placeholder > 0 && $day_placeholder <= $days_in_month )
				$html .= "<td class=\"$cellClass$cellClass2\"><a href=\"$_GETstring\">" . $day_placeholder . "</a></td>";
			else
				$html .= "<td><a href=\"\">&nbsp;</a></td>";
		}
		
		$html .= "</tr>";
	}
	
	echo $html;

}



function getWeeks($year, $month) {
	
	$weeks = 1;
	$days_in_month = cal_days_in_month(CAL_GREGORIAN, date("n", strtotime($month)), date("Y", strtotime($year)));		// number of days in this month
	
	// go through each day of month
	for ($cur_day = 1; $cur_day<=$days_in_month; $cur_day++ ) {
		
		//get day name
		$dayname = date('w', strtotime("$month $cur_day $year"));
		
		// if current day is the 1st day of the week (Sunday), increment $weeks
		if ($dayname == 6 && $cur_day != $days_in_month)
			$weeks++;
	}

	return $weeks;
}


function getEventList( $month, $year, $direction, $numEvents = NULL, $day = NULL, $program = NULL, $venue = NULL, $isToday = false ) {
	
	$events = getEvents();
	$program;
	$html = "";
	$test = "test";
	$curEventNum = 0;
	
	//update month and year
	if ( $direction == "nextMonth" ) {
		$year = getNextYear( $month, $year );
		$month = getNextMonth( $month, $year );
		
	}
	else if ( $direction == "previousMonth" ) {
		$year = getLastYear( $month, $year );
		$month = getLastMonth( $month, $year );
	}
	
	
	//get last month and next month values
	$nextmonth = getNextMonth( $month, $year );
	$nextyear = getNextYear( $month, $year );
	
	$lastyear = getLastYear( $month, $year );
	$lastmonth = getLastMonth( $month, $year );
	$lastmonthdays = cal_days_in_month(CAL_GREGORIAN, date("n", strtotime($lastmonth)), date("Y", strtotime($lastyear)));
	
	
	// update events view title/header
	$program_title = "";
	if ( $program != NULL && $program != "" )
		$program_title = "Program: " . $program;
	$venue_title = "";
	if ( $venue != NULL && $venue != "" )
		$venue_title = "Venue: " . $venue ;
	if ( $day != NULL && $day != "" )
		$day_title = $day . ", " ;
        if ( $isToday )
            $todayString = "Today, ";
        
        
	$html .= '<h4 id="events_viewTitle">' . $todayString . $month . ' ' . $day_title . ' ' . $year . '</h4><p>'. $program_title . "<br>" . $venue_title . '</p>';
            
        if ( $events )
            foreach ($events as $event) {

                    //only show specified number of events
                    /*if ($numEvents != NULL)
                            if ( $curEventNum >= $numEvents )
                                    break;
                    */

                    // get event start and end time
                    $starttime = strtotime($event->getProperty("start time"));
                    $endtime = strtotime($event->getProperty("end time"));

                    // dont display dates ending prior to this date or dates that will start after this date
                    // only show events for this day
                    if ( $day != NULL || $day != "" ) {
                            if ($endtime < strtotime( $month . " " . $day . ", " .  $year) || 
                                            $starttime > strtotime( $month . " " . $day . ", " .  $year . " + 1 day"))
                                    continue;
                    }
                    // only show events for this month
                    else {
                            if ($endtime < strtotime( $lastmonth . " " . $lastmonthdays . ", " .  $lastyear) || 
                                            $starttime > strtotime( $nextmonth . " 1, " .  $nextyear ))
                                    continue;
                    }

                    // only display events with the current selected program
                    if ( $program != NULL && $program != "All CVATD Programs" ) {

                            // parse out department string, for multivalues
                            $departments = explode( ";", $event->getProperty("department") );
                            $departments = array_map('strtolower', $departments);
                            $departments = array_map('trim', $departments);

                            if ($program == "Art" || $program == "Interior Design" || $program == "Art History" || $program == "Art Education" )
                                    $program = "Department of " . $program;
                            else if ( $program == "Dance" || $program == "Theatre" )
                                    $program = "School of " . $program;

                            if ( ! in_array( strtolower($program), $departments ) )
                                    continue;

                    }

                    // only display events with the current selected venue
                    if ($venue != NULL && $venue != "" && $venue != "All CVATD Venues" ) {

                            if ( strpos( strtolower($venue), trim(strtolower($event->getProperty("location")))) === FALSE &&
                                    strpos( trim(strtolower($event->getProperty("location"))), strtolower($venue)) === FALSE
                            )
                                    continue;
                    }

                    $curEventNum++;


                    //shorten event description
                    $descr = $event->getproperty("event description");
                    if ( strlen( $event->getproperty("event description") ) > 200 ) {
                            $descr = strip_tags( substr( $event->getproperty("event description"), 0, 200 ) . " ..." );
                    }

                    // check for image
                    $img = "";
                    if ( $event->getProperty("event image") != "" )
                            $img = "<a href=\"" . site_url . "/news/events/event?eventid=" . $event->getProperty("eventid") . "\"><img src=\"" . $event->getProperty("event image") . "\" alt=\"" . $event->getProperty("title") . "\" width=\"240\"></a>";

                    // check for hosted text
                    $location = "";
                    if ( $event->getProperty("location") != "" )
                            $location = "Hosted at: " . $event->getProperty("location") . "<br>";
                    
                    
                    // if event started before today and has not ended, show today's date instead
                    if ( $starttime < strtotime( "today" ) && $endtime > strtotime( "yesterday" ) )
                        $new_starttime = strtotime( "today" );
                    else
                        $new_starttime = $starttime;
                        

                    $html .= "
                            <!-- start EVENT LIST ITEM -->
                            <div class=\"event-list-item\">
                                    <div class=\"date-box\">
                                                    <span class=\"month-name\">" . date("M", $new_starttime )  . "</span>
                                                    <span class=\"day-number\">" . date("j", $new_starttime ) . "</span>
                                    </div>

                                    <div class=\"event-information\">" .
                                            $img . 
                                            "<h4><a href=\"/events/event?eventid=" . $event->getProperty("eventid") . "\">" . $event->getProperty("title") . "</a></h4>
                                            <p class=\"description\">" . 
                                                    $location . "
                                                    Ends " . date("M j Y", strtotime( $event->getProperty("end time") )) . "<br>" .
                                                    parseDepartments( $event->getProperty("department") )
                                            . "</p>
                                            <p class=\"description2\">" . $descr . "</p>
                                    </div>
                            </div>
                            <!-- end EVENT LIST ITEM -->";
            }
			
	if ( $curEventNum == 0 ) {
		$html .= "<p>There are no events for this date.</p>";
	}
		
	echo "$html";
	
}


function getRecentEventsList() {
		
	//include events
	include "../calendar/events.php";
	
	$events = getEvents();
}



function getNumberEvents( $date, $program = "", $venue = "" ) {
	
	$events = getEvents();
	$numEvents = 0;
	
        if ( $events )
            foreach ($events as $event) {

                    // get start and end times
                    $starttime = strtotime($event->getProperty("start time"));
                    $endtime = strtotime($event->getProperty("end time"));

                    // dont display dates that ended before today or dates that will start after today
                    if ( $endtime < strtotime($date . "00:00:00") || $starttime > strtotime($date . " 23:29:00") )
                            continue;


                    //only display events with the selected program
                    if ($program != "" && $program != "All CVATD Programs" ) {

                            // parse out department string, for multivalues
                            $departments = explode( ";", $event->getProperty("department") );
                            $departments = array_map('strtolower', $departments);
                            $departments = array_map('trim', $departments);

                            if ($program == "Art" || $program == "Interior Design" || $program == "Art History" || $program == "Art Education" )
                                    $program = "Department of " . $program;
                            else if ( $program == "Dance" || $program == "Theatre" )
                                    $program = "School of " . $program;

                            if ( ! in_array( strtolower($program), $departments ) )
                                    continue;


                    }


                    //only display events with the selected venue
                    if ($venue != "" && $venue != "All CVATD Venues" )
                            if ( strtolower($venue) != trim(strtolower($event->getProperty("location"))) )
                                    continue;

                    $numEvents++;
            }
	return $numEvents;
}

function getURLQueryString( $venue, $program, $month, $day_placeholder, $year ) {

	$_GETstring = "";       // holds all html to be output to page at end of event parsing
	$monthHolder = "";	// will be added to end of url, as get variable, if user clicks date in calendar widget table cell 
	$dayHolder = "";	// will be added to end of url, as get variable, if user clicks date in calendar widget table cell 
	$yearHolder = "";	// will be added to end of url, as get variable, if user clicks date in calendar widget table cell 
	$venueHolder = "";	// will be added to end of url, as get variable, if user clicks date in calendar widget table cell 
	$programHolder = "";    // will be added to end of url, as get variable, if user clicks date in calendar widget table cell 

	// check if venue is given; if so, will be added as get variable to end of link url
	if ( $venue != NULL && $venue != "" )
		$venueHolder = ";venue=$venue";
	// check if program given
	if ( $program != NULL && $program != "" )
		$programHolder = ";program=$program";
	// check if month given
	if ( $month != NULL && $month != "" )
		$monthHolder = ";month=" . date("n", strtotime($month));
	// check if month given
	if ( $day_placeholder != NULL && $day_placeholder != "" )
		$dayHolder = ";day=$day_placeholder";
	// check if year given
	if ( $year != NULL && $year != "" )
		$yearHolder = ";year=$year";
		
	$_GETstring = "./index.php?views=$monthHolder$dayHolder$yearHolder$venueHolder$programHolder";
        
        //echo "<script>console.log('" . $_GETstring . "')</script>";
	
	return $_GETstring;
	
}

?>