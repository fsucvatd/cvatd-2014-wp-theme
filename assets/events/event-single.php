

<?php
        $event = $_SESSION['event'];
        ?>
        		
	<h2><?php echo $event->getProperty("title"); ?></h2>
	<div class="feed-date-tags">
		<?php echo date("F d, Y", strtotime($event->getProperty("start time"))); ?> 
		 <span class="del">&bull;</span> 
		<a href=""><?php echo $event->getProperty("department"); ?> </a>
	</div>
	
	<!-- event image -->
	<div id="articleImage">
		<?php 
		if ( $event->getProperty("event image") != "" )
			echo '<img src="' . $event->getProperty("event image") . '" alt="' . $event->getProperty("title") . '" style="max-width:750px;">';
		?>
		<p class="caption"><?php echo $event->getProperty("title"); ?></p>
	</div>
	<!-- END event image -->	
	
	
	
	
		
		
	
	<!-- start event Ticket box -->
	<div id="eventTicketBox">
	
		<div id="date-box-holder">
			<div class="date-box">
				<span class="month-name">
					<?php echo date("M", strtotime($event->getProperty("start time"))); ?></span>
				<span class="day-number">
					<?php echo date("d", strtotime($event->getProperty("start time"))); ?></span>
			</div>
		</div>
		
		
		<div class="event-details">
		
			<h4><a href="<?php echo site_url(); ?>/events/event?eventid=<?php echo $event->getProperty("eventid"); ?>"><?php echo $event->getProperty("title"); ?></a></h4>
			<!--<div class="feed-date-tags">Tagged with: <?php echo parseDepartments( $event->getProperty("department") ); ?></div>-->
			
			<table class="table table-condensed" style="border:none">
				<tr>
					<th scope="row">Time: </th>
					<td><?php echo date("F j, Y<\b\\r>g:i a", strtotime($event->getProperty("start time"))) ;?></td>
				</tr>
				<tr>
					<th scope="row">Ends: </th>
					<td><?php echo date("F j, Y<\b\\r>g:i a", strtotime($event->getProperty("end time"))) ;?></td>
				</tr>
				<tr>
					<th scope="row">Location:</th>
					<td><?php echo $event->getProperty("location"); ?></td>
				</tr>
				<tr>
                                    <td scope="row" colspan="2" style="padding-top:10px;">
                                        <div style="float:right;display:inline-block;font-size: 80%;">
                                            <a style="margin-left:auto;margin-right:auto;" href="<?php echo get_template_directory_uri() . "/assets/events/createICS.php?ds=" . strtotime($event->getProperty("start time") . " + 9 hours") . "&de=" . strtotime($event->getProperty("end time") . " + 9 hours") . "&a=" . $event->getProperty("location") . "&s=" . $event->getProperty("title") . "&u=" . site_url() . "/events/event?eventid=" . $event->getProperty("eventid"); ?>"><img style="clear:both;display:block;margin-left:auto;margin-right:auto;" src="<?php echo get_template_directory_uri() . "/assets/img/export_ical.jpg"; ?>" /></a> 
                                            <a style="margin-left:auto;margin-right:auto;" href="<?php echo get_template_directory_uri() . "/assets/events/createICS.php?ds=" . strtotime($event->getProperty("start time") . " + 9 hours") . "&de=" . strtotime($event->getProperty("end time") . " + 9 hours") . "&a=" . $event->getProperty("location") . "&s=" . $event->getProperty("title") . "&u=" . site_url() . "/events/event?eventid=" . $event->getProperty("eventid"); ?>">Export to iCal</a>
                                        </div>
                                    </td>
				</tr>
			</table>
			
			<?php
			// testing
			// print '<a class="btn btn-carousel" href="#">Buy Tickets Online &raquo;</a>';
			if ( $event->getProperty("ticketlink") != "" )
				echo '<a class="btn btn-carousel" href="' . $event->getProperty("ticketlink") . '" target="_new">Buy Tickets Online &raquo;</a>';
			?>				
		</div>
	
	</div>
	<!-- end event Ticket box -->
	
	
	<!-- start Event Info -->
	<div id="eventinfo">
		<?php echo $event->getProperty("event description"); ?>
	</div>
	<!-- end Event Info -->
		
		