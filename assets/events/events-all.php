<?php
	include get_template_directory() . "/assets/events/getfacilities.php";
	$themesUrl = get_template_directory_uri() . "/assets/events/";
?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/events/css/events.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/events/css/events_mobile.css" />
	<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/events/calendar.js"></script>

	<script>

		// get directory to themes
		themesUrl = '<?php echo $themesUrl ?>';


		

		// get url variables
		<?php 
			$views = '';
			
			// if views is set in url
			if ( isset($_GET['views']) ) $views = $_GET['views'];
		
			// if event_slug set in theme options
			if (get_theme_mod('event_slug','')) $views .= ';program='.get_theme_mod('event_slug',''); 		
		?>
		//var views = "<?php if ( isset($_GET['views']) ) echo ($_GET['views']); else echo ""; ?>";
		var views = "<?php echo $views; ?>";
		

		// get day date
                var todayDate = new Date();

		var month = parseViewData( "month", views );
		var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		var monthName = monthNames[month-1];

		var nextmonthName = monthNames[month];
		var day = parseViewData( "day", views );
		if (day != "") var dayview = "dayView"; else var dayview = "";

		var year = parseViewData( "year", views );

                
                // get other filter views
                var programSelected = parseViewData( "program", views );
                var venueSelected = parseViewData( "venue", views );


		var calendar = new Calendar(dayview, month, day, year, programSelected, venueSelected, monthName);
		var calendar2 = new Calendar("getNextMonth", month, day, year, programSelected, venueSelected, nextmonthName);
                
                
                if (blogname == "FSU College of Visual Arts, Theatre &amp; Dance") {
                    var facilities = <?php echo json_encode($facilities); ?>;
                    var programs = getPrograms();
                }


		function getPrograms() {

			var programs = new Array();
			var programsUnique = new Array();

			//add all/default facilities
			programs.push("All CVATD Programs");

			// get all programs for each facility
			for (var a in facilities) {

				// check if value contains commas aka multi values
				if (facilities[a].program.indexOf(", ") > -1) {

					// separate program (strings values) from commas
					var tempPrograms = facilities[a].program.split(", ");

					// add separated values to main programs array
					for (var b in tempPrograms) {

						//check if main program array already contains this value
						//don't add duplicate values to array
						if (programs.indexOf(tempPrograms[b]) == -1) {

							//trim leading and trailing white space
							var tempProgram = tempPrograms[b].replace(/^\s+|\s+$/g, "");

							programs.push(tempProgram);

						}
					}
				}

				else {
					//trim leading and trailing white space
					var tempProgram = facilities[a].program.replace(/^\s+|\s+$/g, "");

					//check if main program array already contains this value
					//don't add duplicate values to array
					if (programs.indexOf(tempProgram) == -1) {
						program = programs.push(tempProgram);
					}
				}
			}

			//alphabetize array
			programs.sort();

			return programs;
		}

		function parseViewData( property, values ) {

			var returnVal = 0;
			var d = new Date();

			// set today's date
			if ( property == "month")
				returnVal = d.getMonth() + 1;
			else if (property == "day" )
				returnVal = "";
			else if ( property == "year" )
				returnVal = d.getFullYear();
			else if ( property == "program" )
				returnVal = "";

			// if values empty, return default values
			if ( values == "" )
				return returnVal;

			// parse values; based on semicolon
			var valuesArr = values.split(";");

			for ( var a in valuesArr ) {

				// separate property/variable name from value
				var valuesArr2 = valuesArr[a].split("=");
				if ( valuesArr2[0].toLowerCase() == property.toLowerCase() )
					returnVal = valuesArr2[1];

			}

			return returnVal;
		}

	</script>


	<!-- start CALENDAR WIDGET -->
	<div id="calendar-page">

                
            <?php if (get_bloginfo( 'name' ) == "FSU College of Visual Arts, Theatre &amp; Dance") $viewsdisplay = "";
                    else $viewsdisplay = "display:none;";
            ?>
            
		<div id="views" style="<?php echo $viewsdisplay ?>">
			<select id="venue" name="venue" class="drowndown" onChange="calendar.changeProgram($('#program').val(), this.value)">
				<option value="venue" selected disabled>Venue</option>
				<option value="All CVATD Venues">All CVATD Venues</option>
				<script>
				var venueGET = venueSelected.toLowerCase();

				for (var a in facilities) {
					var selected = "";
					if ( facilities[a].name.toLowerCase().indexOf(venueGET) > -1 || facilities[a].shortname.toLowerCase().indexOf(venueGET) > -1 )
                                            selected = " selected";
					document.writeln("<option value=\"" + facilities[a].name + "\"" + selected + ">" + facilities[a].shortname + "</option>");
				}
				</script>
			</select>



			<script>
			var program_temp = " ";
			if (programSelected != null)
				program_temp = programSelected;
			document.write("<select id=\"program\" name=\"program\" class=\"drowndown\" onChange=\"calendar.changeProgram(this.value, $('#venue').val())\">");
                            document.write('<option value="All CVATD Programs">All CVATD Programs</option>');
                            document.write("<option value=\"Department of Art\"")
                            
                            if (program_temp.substr(program_temp.length - 3).toLowerCase() == "art" )
                                document.write( " selected");
                            document.writeln( ">Art</option>");

                        document.write("<option value=\"Department of Art Education\"")
                            if ( program_temp.toLowerCase().indexOf("art education") > -1 || program_temp == "arted" )
                                document.write( " selected");
                            document.writeln( ">Art Education</option>");

                        document.write("<option value=\"Department of Art History\"")
                            if ( program_temp.toLowerCase().indexOf("art history") > -1 || program_temp == "ah" )
                                document.write( " selected");
                            document.writeln( ">Art History</option>");

                        document.write("<option value=\"Department of Interior Design\"")
                            if ( program_temp.toLowerCase().indexOf("interior design") > -1 || program_temp == "id" )
                                document.write( " selected");
                            document.writeln( ">Interior Design</option>");

                        document.write("<option value=\"School of Dance\"")
                            if ( program_temp.toLowerCase().indexOf("dance") > -1 )
                                document.write( " selected");
                            document.writeln( ">Dance</option>");

                        document.write("<option value=\"School of Theatre\"")
                            if (  program_temp.toLowerCase().indexOf("theatre") > -1 )
                                document.write( " selected");
                            document.writeln( ">Theatre</option>");

                        document.write("<option value=\"FSU/Asolo Conservatory\"")
                            if (  program_temp.toLowerCase().indexOf("asolo") > -1 )
                                document.write( " selected");
                            document.writeln( ">FSU/Asolo Conservatory</option>");

                        document.write("<option value=\"Facility for Arts Research\"")
                            if ( program_temp.toLowerCase().indexOf("facility for arts research") > -1 || program_temp.toLowerCase() == "far" )
                                document.write( " selected");
                            document.writeln( ">Facility for Arts Research</option>");

                        document.write("<option value=\"Master Craftsman Studio\"")
                            if ( program_temp.toLowerCase().indexOf("master craftsman") > -1 || program_temp.toLowerCase() == "mcs" )
                                document.write( " selected");
                            document.writeln( ">Master Craftsman Studio</option>");

                        document.write("<option value=\"Museum of Fine Arts\"")
                            if ( program_temp.toLowerCase().indexOf("museum of fine arts") > -1 || program_temp.toLowerCase() == "mofa" )
                                document.write( " selected");
                            document.writeln( ">Museum of Fine Arts</option>");

                        document.write("<option value=\"Ringling\"")
                            if ( program_temp.toLowerCase().indexOf("ringling") > -1 )
                                document.write( " selected");
                            document.writeln( ">Ringling</option>");


			document.write('</select>');

			</script>

                        <input name="today" type="button" value="Today" onClick="window.location.href='../events/?views=month=' + (todayDate.getMonth() +1) + ';day=' + todayDate.getDate() + ';year=' + todayDate.getFullYear() ">
		</div>


		<!-- left column -->
		<div class="left66">

			<script>
			// if initial event page view, print today's events
                        if ( views == "" || views == null ) {
                            document.write( calendar.eventListToday + "<hr style=\"margin: 50px 0;\" \>" );
                        
                            // if no events for today, print this month's events
                            if ( (calendar.eventListToday.toLowerCase().indexOf("no events") > -1 ) )
                                document.write( calendar.eventList );
                        }
                        
                        
			// else, user selected a view, and show all events based on specified view (month, day, year, venue, etc)
                        else {
                            document.write( calendar.eventList );
                        }
			</script>

		</div>
		<!-- end left column -->





	</div>
	<!-- END CALENDAR PAGE -->