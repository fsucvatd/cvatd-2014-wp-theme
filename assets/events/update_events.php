<?php

include "event.php";

$events = parseFile();

// connect to db
$mysql = connectdb();

// get event objects (array)
if ($mysql)
	$events = parseFile();
	
// update/store events into db
if ($events)
	$mysql = updatedb($mysql, $events);

// close db connection
if ($mysql)
	closedb($mysql);


function connectdb() {
	
	$errors = 0;
	$host = "mysql.cvatd.dreamhosters.com";
	$username = "cvatd_db_user";
	$pw = "h#7ELZhRZ*Gh!";
	$dbname = "cvatd_2014_db";
	
	
	// connect to db
	$mysqli = new mysqli($host, $username, $pw, $dbname);
	
	$errors = $mysqli->connect_errno;
	
	if (!$errors)
		return $mysqli;
	else
		return $errors;
}


function parseFile() {
	
	$metadata_children = 8;		// number of extra children, unneccessary metadata
	$a = 0;
	$entries = array();
	$events = array();
	
	
	$feeds = array(
		"%7B99FC8A91-936C-4D24-ADB0-E9023F4CC2A9%7D",			//art & design id
		 "%7B5253E25A-B245-481D-A0DC-BE0121CE0B3B%7D",			// dance id
		 "%7BF8E0EAF9-9979-40E8-90E3-09D4A3A179DB%7D",			// theatre id
		 "%7B7C6AC83C-E5D6-4F76-A3E1-527198F2FAB8%7D");			// mofa id

	// merge all feeds
	foreach ($feeds as $feed) {
		$xml = simplexml_load_file ("http://calendar.fsu.edu/_layouts/listfeed.aspx?List=" . $feed);
   		$entries = array_merge($entries, $xml->xpath('/rss//item'));
	}
	
	// get properties for each child element
	foreach($entries as $child => $element) {
		
		// create new event object & add to $events array
		$events[$a-$metadata_children] = new event();
		
		
		// skip over first 8 children; this is only extra metadata
		foreach( $element as $key => $value ) {
		
				
				//strip element titles
				$element_name = strtolower($value->getName());
				
				//if element title is description, parse children elements
				if ($element_name == "description")
					$events[$a-$metadata_children] = parseElement($events[$a-$metadata_children], $value);
				else
					$events[$a-$metadata_children]->setProperty(strtolower($element_name), $value);
		}
		
		$a++;
	}
	
	//sort by start time
	$events = updateEvents($events);
	
	return $events;
}



function parseElement($event, $element) {
	$newevent = clone $event;		// create a copy of the current event object
	$a = 2;
	$description;
	
	// get all "div" elements within "description" parent element
	preg_match_all('/<div>(.*?)<\/div>/s', $element, $description);
	if (count($description))
		foreach ( $description as $children => $child ){ 
			foreach ($child as $item => $value) {
					
				if(strpos($value, 'Event Description:') !== false)
					$newevent->setProperty( "event description", strstrNode($value));
					
				else if(strpos($value, 'Location:') !== false) {
					$newevent->setProperty( "location", strstrNode($value));
				}
					
				else if(strpos($value, 'Start Time:') !== false)
					$newevent->setProperty( "start time", strstrNode($value));
					
				else if(strpos($value, 'End Time:') !== false)
					$newevent->setProperty( "end time", strstrNode($value));
					
				else if(strpos($value, 'Department:') !== false)
					$newevent->setProperty( "department", strstrNode($value));
					
				else if(strpos($value, 'Event Image:') !== false) {
					$dom = new domDocument;
					$dom->loadHTML($value);
					$dom->preserveWhiteSpace = false;
					$images = $dom->getElementsByTagName('a');
					foreach ($images as $image)
						$newevent->setProperty( "event image", $image->getAttribute('href'));
				}
				else if(strpos($value,'Contact Person:') !== false)
					$newevent->setProperty( "contact person", strstrNode($value));
			}
			
			$a++;
		}
		
	return $newevent;
}


function updatedb($mysqli, $events) {
	
	$tablename = "events";
	$errors = 0;
	$save_point = NULL;
	
	
	/* set autocommit to off */
	$mysqli->autocommit(FALSE);

	//drop all events table/database	
	if (!$errors && !$mysqli->query("TRUNCATE TABLE $tablename"))
			$errors = $mysqli->errno;
			
	foreach ($events as $event) {
		
		//prepare
		$query = "INSERT INTO events(title, location, starttime, endtime, description, program, imageurl, contact, author, category, publishdate, link) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		if(!$errors)
			if (!($stmt = $mysqli->prepare($query)))
				$errors = $mysqli->errno;
		
		// bind parameters
		if(!$errors)
			$stmt->bind_param("ssssssssssss", 
				$title,
				$location,
				$starttime,
				$endtime,
				$description,
				$program,
				$imageurl,
				$contact,
				$author,
				$category,
				$publishdate,
				$link);
		
		
		//assign bound parameters
		$title = $event->getProperty("title") . "";
		$location = $event->getProperty("location") . "";
		$starttime = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $event->getProperty("start time"))));
		$endtime = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $event->getProperty("end time"))));
		$description = $event->getProperty("event description") . "";
		$program = $event->getProperty("department") . "";
		$imageurl = $event->getProperty("event image") . "";
		$contact = $event->getProperty("contact person") . "";
		$author = $event->getProperty("author") . "";
		$category = $event->getProperty("category") . "";
		$publishdate = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $event->getProperty("pubdate"))));
		$link = $event->getProperty("link") . "";
		
		//execute
		if(!$errors && !$stmt->execute())
				$errors = $mysqli->errno;
		
		/* close statement */
		if(!$errors && !$stmt->close())
				$errors = $mysqli->errno;
	}
	
	
	/* commit transaction */
	(!$errors) ? $mysqli->commit() : $mysqli->rollback();
	
}


function closedb($mysqli) {
	
	/* close connection */
	$mysqli->close();	
}


function updateEvents($events) {
	
	$a = 0;
	
	//sort events
	foreach($events as &$event)
		$tmp[] = strtotime($event->getProperty("end time"));
		
	array_multisort($tmp, SORT_ASC, $events);	
	
	$events = array_values($events);
	
	return $events;
}

function strstrNode($value) {
	
	$value = strstr($value, ':');
	return strstr($value, ' ');
}


?>