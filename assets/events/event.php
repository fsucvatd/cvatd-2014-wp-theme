<?php

class event {
	
	private $eventid,
			$title,
			$link,
			$location,
			$starttime,
			$endtime,
			$description,
			$program,
			$imageurl,
			$contact,
			$author,
			$category,
			$ticketlink,
			$publishdate;
			
	function setProperty ($property, $value) {
		switch ($property) {
			case "eventid":
				$this->eventid = $value; break;
			case "title":
				$this->title = $value; break;
			case "link":
				$this->link = $value; break;
			case "location":
				$this->location = $value; break;
			case "start time":
				$this->starttime = $value; break;
			case "end time":
				$this->endtime = $value; break;
			case "event description":
				$this->description = $value;
				
				// parse & set ticketlink
				$ticketlink = "";
				
				// go through each line
				$lines = explode("\n", $value);
				foreach ( $lines as $line )
				
					// if a purchase ticket link is provided, extract link & assign to ticketlink
					if ( strpos( $line, "Purchase Tickets: h" ) !== false )
						$ticketlink = str_replace( "Purchase Tickets: ", "", $line );
						
				$this->ticketlink = $ticketlink;
				
				break;
			case "department":
				$this->program = $value; break;
			case "event image":
				$this->imageurl = $value; break;
			case "contact person":
				$this->contact = $value; break;
			case "author":
				$this->author = $value; break;
			case "category":
				$this->category = $value; break;
			case "pubdate":
				$this->publishdate = $value; break;
		}
	}
	
	function getProperty($property) {
		
		$value;
		
		switch ($property) {
			case "eventid":
				$value = $this->eventid; break;
			case "title":
				$value = $this->title; break;
			case "link":
				$value = $this->link; break;
			case "location":
				$value = $this->location; break;
			case "start time":
				$value = $this->starttime; break;
			case "end time":
				$value = $this->endtime; break;
			case "event description":
				$value = $this->description; break;
			case "department":
				$value = $this->program; break;
			case "event image":
				$value = $this->imageurl; break;
			case "contact person":
				$value = $this->contact; break;
			case "author":
				$value = $this->author; break;
			case "category":
				$value = $this->category; break;
			case "pubdate":
				$value = $this->publishdate; break;
			case "ticketlink":
				$value = $this->ticketlink; break;
		}
		
		return $value;
	}
}
?>