<?php

include "googledoc.php";
include "facility.php";

$DOCURL = "https://docs.google.com/spreadsheet/pub?key=0AiMsnJ7guD8BdExsRkRKWEtjRjZ2VWhLUjgzNGdxbmc&single=true&gid=0&output=csv";

// create new google doc
$document = new GoogleDoc($DOCURL);

//open document and parse content rows into array
$data = $document->openFile();

//parse data into objects
$facilities = parseFacilities($data);

// get facility data
$facilities = convertToArray($facilities);

return $facilities;


function parseFacilities($data) {
	
	$facilities = array();
	$a = 0;
	$b;
	
	foreach ($data as $facility => $row) {
		
		//skip first row, which is column headings
		if ($a == 0) {
			$a++;
			continue;
		}
		
		$b = $a - 1;
		
		//create new facility object
		$facilities[$b] = new Facility();
		
		// set facility properties
		$facilities[$b]->setProperty("id", $row[0]);
		$facilities[$b]->setProperty("name", $row[1]);
		$facilities[$b]->setProperty("abbreviation", $row[2]);
		$facilities[$b]->setProperty("shortname", $row[3]);
		$facilities[$b]->setProperty("description", $row[4]);
		$facilities[$b]->setProperty("address", $row[5]);
		$facilities[$b]->setProperty("website", $row[6]);
		$facilities[$b]->setProperty("latitude", $row[7]);
		$facilities[$b]->setProperty("longitude", $row[8]);
		$facilities[$b]->setProperty("imageurls", $row[9]);
		$facilities[$b]->setProperty("type", $row[10]);
		$facilities[$b]->setProperty("program", $row[11]);
		$facilities[$b]->setProperty("phone", $row[12]);
		$facilities[$b]->setProperty("contact", $row[13]);
		$facilities[$b]->setProperty("mailcode", $row[14]);
		
		// go to next row/facility
		$a++;
	}
	
	return $facilities;
}


function displayFacilities($facilities) {
	
	foreach ($facilities as $facility) {
		echo "<h4>" . $facility->getProperty("name") . "</h4>" .
			"<p>" . "Abbreviation: " . $facility->getProperty("abbreviation") . "<br/>" .
					"Description: " . $facility->getProperty("description") . "<br/>" .
					"Address: " . $facility->getProperty("address") . "<br/>" .
					"Website: " . $facility->getProperty("website") . "<br/>" .
					"Latitude: " . $facility->getProperty("latitude") . "<br/>" .
					"Longitude: " . $facility->getProperty("longitude") . "<br/>" .
					"Images: " . var_dump($facility->getProperty("imageurls")) . "<br/>" .
					"Type: " . $facility->getProperty("type") . "<br/>" .
					"Program: " . $facility->getProperty("program") . "<br/>" .
					"Phone: " . $facility->getProperty("phone") . "<br/>" .
					"Contact: " . $facility->getProperty("contact") . "<br/>" .
					"Mailcode: " . $facility->getProperty("mailcode") . "<br/>";
	}
}

function convertToArray( $facilities ) {
	
	$facilitesArray = array();
	$a = 0;
	
	foreach ($facilities as $facility) {
		$facilitesArray[$a]['name'] = $facility->getProperty("name");
		$facilitesArray[$a]['abbreviation'] = $facility->getProperty("abbreviation");
		$facilitesArray[$a]['shortname'] = $facility->getProperty("shortname");
		$facilitesArray[$a]['description'] = $facility->getProperty("description");
		$facilitesArray[$a]['address'] = $facility->getProperty("address");
		$facilitesArray[$a]['website'] = $facility->getProperty("website");
		$facilitesArray[$a]['latitude'] = $facility->getProperty("latitude");
		$facilitesArray[$a]['longitude'] = $facility->getProperty("longitude");
		$facilitesArray[$a]['imageurls'] = $facility->getProperty("imageurls");
		$facilitesArray[$a]['type'] = $facility->getProperty("type");
		$facilitesArray[$a]['program'] = $facility->getProperty("program");
		$facilitesArray[$a]['phone'] = $facility->getProperty("phone");
		$facilitesArray[$a]['contact'] = $facility->getProperty("contact");
		$facilitesArray[$a]['mailcode'] = $facility->getProperty("mailcode");
		
		$a++;
	}
	
	return $facilitesArray;
}

?>