<?php

require_once "event.php";

function getEvents() {
	

	$events; //array to store events, represented as objects	
	
	// connect to db
	$mysql = connectdb();
	
	// get events from db
	if ($mysql)
		$events = selectEvents($mysql, $events);
		
	// close db connection
	if ($mysql)
		closedb($mysql);
		
	return $events;
}

function getEvent($eventid) {
	
	$event; //array to store event objects
	
	// connect to db
	$mysql = connectdb();
	
	// get event from db
	if ($mysql)
		$event = selectEvent($mysql, $eventid);
		
	// close db connection
	if ($mysql)
		closedb($mysql);
		
	return $event;
}

function connectdb() {
	
	$errors = 0;
	$host = "mysql.cvatd.dreamhosters.com";
	//$host = "localhost";
	$username = "cvatd_db_user";
	//$username = "";
	$pw = "h#7ELZhRZ*Gh!";
	//$pw = "";
	$dbname = "cvatd_2014_db";
	
	
	// connect to db
	$mysqli = new mysqli($host, $username, $pw, $dbname);
	
	$errors = $mysqli->connect_errno;
	
	if (!$errors)
		return $mysqli;
	else
		return $errors;
}

function selectEvents($mysqli, $events) {
	
	$tablename = "events";
	$errors = 0;
	$save_point = NULL;
	$rows; // array to hold each event returned from db query
	

	//get events from db & store all returned data into $rows
	if ($result = $mysqli->query("SELECT * FROM " . $tablename . " ORDER BY starttime"))
		while ($row = mysqli_fetch_assoc($result))
			$rows[] = $row;
	
	// parse returned event data into $events array objects
	for($a=0; $a<count($rows); $a++) {
		
		// create new event object & add to $events array
		$events[$a] = new event();
		
		//set object member data
		$events[$a]->setProperty("eventid", $rows[$a]['eventid']);
		$events[$a]->setProperty("title", $rows[$a]['title']);
		$events[$a]->setProperty("link", $rows[$a]['link']);
		$events[$a]->setProperty("location", $rows[$a]['location']);
		$events[$a]->setProperty("start time", $rows[$a]['starttime']);
		$events[$a]->setProperty("end time", $rows[$a]['endtime']);
		$events[$a]->setProperty("event description", $rows[$a]['description']);
		$events[$a]->setProperty("department", $rows[$a]['program']);
		$events[$a]->setProperty("event image", $rows[$a]['imageurl']);
		$events[$a]->setProperty("contact person", $rows[$a]['contact']);
		$events[$a]->setProperty("author", $rows[$a]['author']);
		$events[$a]->setProperty("category", $rows[$a]['category']);
		$events[$a]->setProperty("pubdate", $rows[$a]['publishdate']);
	}

        /* free result set */
        if ( $result )
            $result->close();
	
	
	//var_dump($events);
		
	return $events;	
}

function selectEvent($mysqli, $eventid) {
	
	require_once get_template_directory() . "/assets/events/event.php";

	$tablename = "events";
	$errors = 0;
	$save_point = NULL;
	$row; // array to hold each event returned from db query
	
	
	// if no eventid provided, return 0
	if ( !$eventid || $eventid == NULL )
		return 0;

	//get events from db & store all returned data into $rows
	if ($result = $mysqli->query("SELECT * FROM " . $tablename . " WHERE eventid=" . $eventid))
		$row = mysqli_fetch_assoc($result);
		
		
	// create new event object & add to $events array
	$event = new event();
	
	//set object member data
	$event->setProperty("eventid", $row['eventid']);
	$event->setProperty("title", $row['title']);
	$event->setProperty("link", $row['link']);
	$event->setProperty("location", $row['location']);
	$event->setProperty("start time", $row['starttime']);
	$event->setProperty("end time", $row['endtime']);
	$event->setProperty("event description", $row['description']);
	$event->setProperty("department", $row['program']);
	$event->setProperty("event image", $row['imageurl']);
	$event->setProperty("contact person", $row['contact']);
	$event->setProperty("author", $row['author']);
	$event->setProperty("category", $row['category']);
	$event->setProperty("pubdate", $row['publishdate']);
	
    /* free result set */
    $result->close();
	
	
	//var_dump($events);
		
	return $event;	
}

function closedb($mysqli) {
	
	/* close connection */
	$mysqli->close();	
}

function displayEvents($events) {
	
	foreach($events as $event) {
		echo "<h2>" . $event->getProperty("title") . "</h2>";
		echo "<br />link: " . $event->getProperty("link");
		echo "<br />location: " . $event->getProperty("location");
		echo "<br />start time: " . $event->getProperty("start time");
		echo "<br />end time: " . $event->getProperty("end time");
		echo "<br />event description: " . $event->getProperty("event description");
		echo "<br />department: " . $event->getProperty("department");
		echo "<br />event image: <img alt=\"" . $event->getProperty("title") . "\" src=\"" . $event->getProperty("event image") . "\" style=\"clear:right;\"";
		echo "<br /><br />contact person: " . $event->getProperty("contact person");
		echo "<br />author: " . $event->getProperty("author");
		echo "<br />category: " . $event->getProperty("category");
		echo "<br />pubdate: " . $event->getProperty("pubdate");
	}
}

function updateEvents($events) {
	
	$a = 0;
	
	//sort events
	foreach($events as &$event)
		$tmp[] = strtotime($event->getProperty("end time"));
		
	array_multisort($tmp, SORT_ASC, $events);	
	
	$events = array_values($events);
	
	return $events;
}

function strstrNode($value) {
	
	$value = strstr($value, ':');
	return strstr($value, ' ');
}

?>
