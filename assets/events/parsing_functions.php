<?php



function parseDepartments( $departmentsString ) {

	$html = "";
	$departments = explode( ";", $departmentsString );
	$numDepartments = count($departments);
	$curDeptNum = 1;
	
	// go thru each dept from array
	foreach( $departments as $department ) {
		
		$deptDelimeter = ", ";
		
		// check if last dept in array
		if ( $curDeptNum >= $numDepartments )
			$deptDelimeter = "";
		
		$html .= "<span class=\"program-name\"><a href=\"http://" . getDeptLinks($department) . "\" class=\"greylink\">" . abrvDept($department) . "</a>$deptDelimeter</span> ";
		$curDeptNum++;
	}
	
	return $html;
}



function abrvDept( $department ) {
	
	$abrv = "";
	
	if ( strpos($department, "Art Education") ) 
		$abrv = "Art Education";
		
	else if ( strpos($department, "Art History") ) 
		$abrv = "Art History";
		
	else if ( strpos($department, "Interior Design") ) 
		$abrv = "Interior Design";
		
	else if ( strpos($department, "Dance"))
		$abrv = "Dance";
                
        else if ( strpos($department, "Maggie") || strpos($department, "MANCC") ) 
		$abrv = "MANCC";
		
	else if ( strpos($department, "Asolo") ) 
		$abrv = "Asolo Conservatory";
		
	else if ( strpos($department, "Theatre") ) 
		$abrv = "Theatre";
		
	else if ( strpos($department, "Master Craftsman") || strpos($department, "MCS") ) 
		$abrv = "MCS";
		
	else if ( strpos($department, "Ringling") ) 
		$abrv = "Ringling";
		
	else if ( strpos($department, "Facility for Arts Research") || strpos($department, "FAR") ) 
		$abrv = "FAR";
		
	else if ( strpos($department, "Museum of Fine Arts") || strpos($department, "MoFA") || strpos($department, "MOFA") ) 
		$abrv = "MoFA";
		
	else if ( strpos($department, "Art") ) 
		$abrv = "Art";
		
	return $abrv;
}


function getDeptLinks( $department ) {

	$link = "";
	
	if ( strpos($department, "Art Education") ) 
		$link = "arted.fsu.edu";
		
	else if ( strpos($department, "Art History") ) 
		$link = "arthistory.fsu.edu";
		
	else if ( strpos($department, "Interior Design") ) 
		$link = "interiordesign.fsu.edu";
		
	else if ( strpos($department, "Dance"))
		$link = "mancc.fsu.edu";
                
        else if ( strpos($department, "Maggie") || strpos($department, "MANCC") ) 
		$link = "mancc.org";
		
	else if ( strpos($department, "Asolo") ) 
		$link = "asolorep.org";
		
	else if ( strpos($department, "Theatre") ) 
		$link = "theatre.fsu.edu";
		
	else if ( strpos($department, "Master Craftsman") || strpos($department, "MCS") ) 
		$link = "craft.fsu.edu";
		
	else if ( strpos($department, "Ringling") ) 
		$link = "ringling.org";
		
	else if ( strpos($department, "Facility for Arts Research") || strpos($department, "FAR") ) 
		$link = "artsresearch.fsu.edu";
		
	else if ( strpos($department, "Museum of Fine Arts") || strpos($department, "MoFA") || strpos($department, "MOFA") ) 
		$link = "mofa.fsu.edu";
		
	else if ( strpos($department, "Art") ) 
		$link = "art.fsu.edu";
		
	return $link;
		
}

?>