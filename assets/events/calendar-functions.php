<?php

//include main calendar functions
include("calendar.php");

if ($_POST['content'] == "calendarHeader")
	changeCalendarHeader($_POST['month'], $_POST['year'], $_POST['direction'] );

else if ($_POST['content'] == "CalendarTableCells") {
	displayCalendarMonth( $_POST['month'], $_POST['year'], $_POST['direction'], $_POST['program'], $_POST['venue'], $_POST['day'] );
}

else if ($_POST['content'] == "calendarlist") {
    
    $isToday = false;
    if (isset($_POST['isToday'])) {
        $isToday = $_POST['isToday'];
        $_POST['month'] = date('F');
        $_POST['day'] = date('j');
        $_POST['year'] = date('Y');
    }
    getEventList( $_POST['month'], $_POST['year'], $_POST['direction'], 10,  $_POST['day'], $_POST['program'], $_POST['venue'], $isToday );
}

else  {
	
}
	
return 0;

?>