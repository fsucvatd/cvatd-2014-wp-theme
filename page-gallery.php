<?php
/**
   Template Name: Gallery Page
 * The Template for displaying all posts from a specified gallery.
 */
get_header(); ?>

<div class="container top-container">



	<div class="row">
		<div class="col-sm-3">
			<?php print_subnav($post); ?>
		</div>
	

		<div class="col-sm-9">
			
			<?php echo get_breadcrumb(); ?>
		
			<h2><?php print get_the_title($post->ID); ?></h2>			
			
				
				
			<div class="feed cvatd-gallery">
				
			<?php
			// get posts
			// http://codex.wordpress.org/Template_Tags/get_posts
			// get category, using page name
			$categories_objs = get_the_category();
			$categories_arr = array();
			$categories = 0;
                        $exclude_categories_str = "";
			
			foreach ( $categories_objs as $category_obj )
				$categories_arr[] = $category_obj->cat_ID;
                        
                        // if multiple categories, exclude main "Gallery" category
                        if ( count( $categories_arr ) > 1 ) {
                            
                            $excluded_categories_arr = array();
                            
                            // get Gallery category ID
                            $excluded_categories_arr[] = get_cat_ID( "Gallery" );
                            $excluded_categories_arr[] = get_cat_ID( "gallery" );
                            $excluded_categories_arr[] = get_cat_ID( "GALLERY" );
                            
                            foreach( $excluded_categories_arr as $excluded_category ) {
                                $exclude_key = array_search($excluded_category, $categories_arr);
                                
                                if ( isset($exclude_key) && $exclude_key !== FALSE )
                                    unset( $categories_arr[$exclude_key] );
                            }
                        }
                        
			if ( ! empty($categories_arr) )
                            $categories = implode(",", $categories_arr); 
			
			$args = array (
				'posts_per_page'   => 10,
				'offset'           => 0,
				'paged'            => ( get_query_var('paged') ) ? get_query_var('paged') : 1,
				'category'         => $categories,
				'orderby'          => 'post_date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '52',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'post',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'post_status'      => 'publish',
			);
			$posts = get_posts($args);
			if ($posts) {
				
				// get number of posts
				$numPosts = count($posts);
				
				// define number of columns in each row
				$total_columns = 4;
				
				// get number of rows with 4 columns/gallery items
				$numrows = ceil( $numPosts/$total_columns);
				
				// set/start current row and column
				$cur_row = 0;
				$cur_col = 0; ?>
				<?php
				foreach($posts as $post) {
					$newRow = false;
					
					// start new row, if end of column
					if ( $cur_col == 0 ) { 
						$newRow = true; ?>
						<div class="row gallery-row">
					<?php
					} /// end if ( $cur_col < $total_columns )
					
					
					// setup global post data
					setup_postdata($post);
					?>
                                                    
                                        <div class="gallery-column col-md-3">
                                            <?php
                                            //class="img-responsive"$attr = array(
                                            // check for set featured image
                                            if ( strlen( $img = get_the_post_thumbnail( $post->ID, array( 180, 160, ) ) ) )
                                                
                                                $feat_img = get_the_post_thumbnail($post->ID, array( 180, 160, ));

                                            // if no featured image, get first image in post
                                            else
                                                    $feat_img = '<img src="http://new.cvatd.fsu.edu/wp-content/uploads/2014/02/CVATD-1.jpg">';
                                                    
                                            $t = '<a class="image-holder" href="'. get_permalink( $post->ID ) .'" ';
                                            $t .= 'title="' . esc_attr( $post->post_title ) . '">';
                                            $t .= $feat_img;
                                            $t .= '</a>';
                                            $t .= '<a href="'. get_permalink( $post->ID ) .'" ';
                                            $t .= 'title="' . esc_attr( $post->post_title ) . '">';
                                            $t .= '<div class="gallery-cvatd-caption">' . $post->post_title . '</div>';
                                            $t .= '</a>';
                                            print $t;

                                            ?>	
                                        </div>	 <!-- end <div class="gallery-column col-md-3"> -->
                                        
                                                <div class="clearfix visible-md"></div> <!-- clear the XS cols if their content doesn't match in height -->
                        				
						
					<?php
					// start new row, if end of row
					if ( $cur_col == 3 ) {
						$cur_col = 0; ?>
						</div> <!-- end <div class="gallery-cvatd row"> -->
					<?php
					} // end if ( $cur_col == 3 )
					
					else {
						$cur_col++;
					}  // end else from if ( $cur_col == 3 )
					
				} // end foreach($posts as $post) ?>
		
			
				<?php
			
			} // end if ($posts)
			?>
			</div> <!-- end <div class="feed"> -->
		
		
			
		</div>
			
			
		</div>

  </div>
  
		
</div>

<?php get_footer(); ?>