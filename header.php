<?php  //add_filter('show_admin_bar', '__return_false'); // hide admin bar ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<title><?php echo get_bloginfo( 'name' ); ?></title>
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/events/css/event.css" />
	
        <!-- include google analytics tracking -->
        <?php include_once("analyticstracking.php") ?>
        <!-- end analytics tracking -->
        
        <!-- favicon -->
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
        <!-- end favicon -->
        
        
        <!-- social graph -->
        <?php

        $title = get_bloginfo( 'name' );
        $img_url = get_theme_mod( 'logo_textbox') /*"http://new.cvatd.fsu.edu/wp-content/uploads/2014/04/seal-copy.jpg"*/;
        $postcontent = get_theme_mod( 'blogdescription');

        // display social meta tags; only for non-event pages, who have their own config
        if ( strpos( $_SERVER['REQUEST_URI'], '/events/') === FALSE ) { 
            
            // set title
            if ( strlen(get_the_title($post->ID)) > 4 )
                $title = get_the_title($post->ID);


            // check for any images
            if ( strlen( get_the_post_thumbnail($post->ID) ) ) {            
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
                $img_url = $thumb[0];
            }
            
        } // else display special tags for event pages
        else if ( strpos( $_SERVER['REQUEST_URI'], '/events/') !== FALSE && strpos( $_SERVER['REQUEST_URI'], '?eventid=') !== FALSE ) {
            // get event
            $event = theme_getEvent();
            $_SESSION['event'] = $event;
            
            // set title
            $title = $event->getProperty("title");

            // set post content
            $postcontent = shorten_post_content($event->getProperty("event description"),180);

            // check for any images
            if ( strlen( $event->getProperty("event image") ) )         
                $img_url = $event->getProperty("event image");
        
        }?>
        
                
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:description" content="<?php echo get_theme_mod('description_textbox') ?>" />
        <meta property="og:image" content="<?php echo $img_url; ?>" />
        <meta property="og:site_name" content="<?php echo get_bloginfo( 'name' ) ?>" />
        <meta property="og:type" content="website" />
        <!-- end social graph -->
        
	<?php wp_head(); ?>
</head>
<body>

<div id="navcontainer">
    <div class="cvatd-red-bar"></div>



    <div class="container" >
            <nav class="navbar navbar-cvatd" role="navigation">
                    <!-- group brand and toggle for better mobile display -->
                    <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                            </button>
                            <?php if ( get_theme_mod( 'logo_textbox') ) { ?>
                                <a class="navbar-brand" href="<?php echo site_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo get_theme_mod( 'logo_textbox')?>" class="img-responsive" alt="<?php bloginfo('name'); ?> logo"></a>
                            <?php } ?>
                    </div><!-- /navbar-header -->


                    <?php

                    /**
                     *	Create two menus
                     *	1. one for xs screens
                     * 	2. another horizontal one for everything else
                     */
                    $str_arr_xs = array(); // store all the strings for menu for xs screen
                    $str_arr_lg = array(); // store all the strings for menu for sm, md, lg screens
                    $parents = om_get_parent_pages('page');	// get all parent pages
                    foreach( $parents as $parent ) {	

                            // get children
                            $children = om_get_child_pages($parent->ID,'page');

                            // look at ancestors
                            $family = array_reverse(get_post_ancestors($post->ID));
                            if (isset($family[0])) $family_parent = get_post($family[0]);
                            if (isset($family[1])) $family_gparent = get_post($family[1]);

                            // TOP LEVEL 
                            if ($post->post_parent == 0 && $parent->ID == $post->ID){
                                    $str_lg = '<li class="active">';
                            // TOP LEVEL -> CHILD	
                            } else if (count($family) > 0 && $family_parent->ID == $parent->ID){
                                    $str_lg = '<li class="active">';
                            // TOP LEVEL -> CHILD -> CHILD
                            } else if (count($family) > 1 && $family_gparent->ID == $parent->ID){
                                    $str_lg = '<li class="active">';
                            } else {
                                    $str_lg = '<li>';
                            }

                            // loop and create heading first
                            $str_lg .= '<a href="'. get_page_link($parent->ID) .'">'. $parent->post_title .'</a>';
                            $str_xs = $str_lg;

                            // if there are children... 
                            if ($children){ 

                                    // ...reset $str_xs because it needs a different html structure
                                    $str_xs = '<li class="dropdown">'; 
                                    $str_xs .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu_'.str_replace("-", "+",$parent->post_name).'">';
                                    $str_xs .= $parent->post_title .' <b class="caret"></b></a>';

                                    // ... and make a sub menu
                                    $str_lg .= '<ul>';
                                    $str_xs .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu_'.str_replace("-", "+",$parent->post_name).'">';
                                    $link = '';

                                    // loop through children and make links
                                    foreach($children as $child){ 
                                            $link .= '<li><a href="'.get_page_link($child->ID).'">'.$child->post_title.'</a></li>';	
                                    }
                                    $str_lg .= $link.'</ul>';
                                    $str_xs .= $link.'</ul>';
                            }
                            $str_lg .= '</li>';
                            $str_xs .= '</li>';

                            // push string onto end of each array
                            array_push($str_arr_xs,$str_xs);
                            array_push($str_arr_lg,$str_lg);
                    }	

                    ?>

                    <div class="collapse hidden-lg hidden-md hidden-sm" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                    <?php print implode('',$str_arr_xs); ?>
                            </ul>
                    </div>


                    <!-- mobile site collapse -->
                    <div class="navbar-collapse hidden-xs" >
                            <!--<input type="text" class="form-control" placeholder="search" id="srch-term">-->
                            <ul class="nav navbar-nav navbar-right">

                                    <?php print implode('',$str_arr_lg); ?>


                                    <!-- search bar -->
                                    <li id="searchcontainer" class="search<?php if (isset($_GET['s']) && $_GET['s'] != '') print " active"; ?>" onMouseOver="focusSearchInput()" onMouseOut="hideSearchInput()">
                                        <a><i class="glyphicon glyphicon-search"></i></a>
                                            <ul id="searchinputcontainer">
                                                    <li style="width:300px"><form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>"><input type="text" class="form-control" placeholder="search" name="s" id="s"></form></li>





                                            </ul>
                                    </li>
                                    <!-- end search bar -->

                            </ul>

<script>
function focusSearchInput() {
	$( "#s" )
		.focus()
		.blur( function(){
			$("#searchinputcontainer").css("display", "none");});
}
function hideSearchInput() {
	$( "#searchinputcontainer" ).css("display", "block");
}
</script>
                    </div><!-- /.navbar-collapse -->
            </nav>
    </div><!-- /.container -->

</div>

<!--
<div id="nav-spacer">
&nbsp;
</div>
-->
<script>
    changeNavSize();
    window.addEventListener('resize', function(event){
        changeNavSize();
    });
    function changeNavSize() {
        var ht = parseInt(document.getElementById("navcontainer").clientHeight);
        document.getElementById("nav-spacer").setAttribute("style", "height:" + ht + "px");
    }
</script>