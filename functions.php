<?php 

//add_filter('show_admin_bar', '__return_false');


/** 
 *	Load scripts the right way
 *	putting scripts here ensures they are loaded with wp_head() 
 *	and resolves conflicts (e.g. versions of jquery, order of scripts loaded)
 */
function wpbootstrap_scripts_with_jquery()
{
	$theme = get_template_directory_uri();

	// bootstrap
	wp_register_script('bootstrap',$theme.'/assets/js/bootstrap.min.js',array('jquery'),'',false);
	wp_enqueue_script('bootstrap');
	// film roll
	wp_register_script('filmroll',$theme.'/assets/js/jquery.film_roll.min.js',array('jquery'),'',false);
	wp_enqueue_script('filmroll');
	// om_functions
	wp_register_script('om_functions',$theme.'/assets/js/om_functions.js',array('jquery'),'',false);
	wp_enqueue_script('om_functions');

}

add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function customizer( $wp_customize ) {
    
    
    /* Description settings */
    $wp_customize->add_section(
        'section_2',
        array(
            'title' => 'Description Settings',
            'description' => 'Add a description for users of your site.',
            'priority' => 35,
        )
    );
    
    $wp_customize->add_setting('description_textbox',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control('description_textbox',
        array(
            'label' => 'Site Description',
            'section' => 'section_2',
            'type' => 'text',
        )
    );
    
    /* Events Slug settings */
    $wp_customize->add_setting('event_slug',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control('event_slug',
        array(
            'label' => 'Events slug',
            'section' => 'section_2',
            'type' => 'text',
        )
    );
	
	
    
    /* header logo settings */
    $wp_customize->add_section(
        'section_l',
        array(
            'title' => 'Logo Settings',
            'description' => 'Customize the logo for your theme.',
            'priority' => 36,
        )
    );
    
    $wp_customize->add_setting(
        'logo_textbox',
        array(
            'default' => 'http://',
        )
    );
    
    $wp_customize->add_control(
        'logo_textbox',
        array(
            'label' => 'Logo URI',
            'section' => 'section_l',
            'type' => 'text',
        )
    );
    
    
    /* footer logo settings */
    $wp_customize->add_setting(
        'footerimg_textbox',
        array(
            'default' => 'http://',
        )
    );
    
    $wp_customize->add_control(
        'footerimg_textbox',
        array(
            'label' => 'Footer Image URI',
            'section' => 'section_l',
            'type' => 'text',
        )
    );
    
    
    /* social media settings */
    $wp_customize->add_section(
        'section_socialmedia',
        array(
            'title' => 'Social Media Settings',
            'description' => 'Connect your social media with the theme.',
            'priority' => 37,
        )
    );
    
    $wp_customize->add_setting(
        'facebook_uri',
        array(
            'default' => 'http://',
        )
    );
    $wp_customize->add_control(
        'facebook_uri',
        array(
            'label' => 'Facebook URI',
            'section' => 'section_socialmedia',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'linkedin_uri',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'linkedin_uri',
        array(
            'label' => 'Linkedin URI',
            'section' => 'section_socialmedia',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'twitter_uri',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'twitter_uri',
        array(
            'label' => 'Twitter URI',
            'section' => 'section_socialmedia',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'youtube_uri',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'youtube_uri',
        array(
            'label' => 'YouTube URI',
            'section' => 'section_socialmedia',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'newsletter_uri',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'newsletter_uri',
        array(
            'label' => 'Newsletter URI',
            'section' => 'section_socialmedia',
            'type' => 'text',
        )
    );
}
add_action( 'customize_register', 'customizer' );
	
	

//post thumbnail support
add_action( 'after_setup_theme', 'theme_setup' );

function theme_setup() {
	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(200, 150, true); // default Post Thumbnail dimensions (cropped)
		
		// additional image sizes
		// delete the next line if you do not need additional image sizes
		add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
		add_image_size( 'sidebar-thumb', 120, 120, true );
		add_image_size( 'homepage-thumb', 300, 200, true );
		add_image_size( 'singlepost-thumb', 590, 9999 );
	}
}


function wpb_widgets_init() {

	/**
	 *	Footer rows
	 */
	register_sidebar( array(
		'name' => __( 'Footer Row #1', 'wpb' ),
		'id' => 'footer-row-1',
		'description' => __( 'The first footer row widget area', 'wpb' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Row #2', 'wpb' ),
		'id' => 'footer-row-2',
		'description' => __( 'The second footer row widget area', 'wpb' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Row #3', 'wpb' ),
		'id' => 'footer-row-3',
		'description' => __( 'The second footer row widget area', 'wpb' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	/**
	 *	Sidebars
	 */
	register_sidebar( array(
		'name' =>__( 'Front page sidebar', 'wpb'),
		'id' => 'sidebar-right-1',
		'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' =>__( 'Sidebar for news pages', 'wpb'),
		'id' => 'sidebar-news-page',
		'description' => __( 'Appears in the left column of news pages', 'wpb' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' =>__( 'Sidebar for events pages', 'wpb'),
		'id' => 'sidebar-events-page',
		'description' => __( 'Appears in the column of events pages', 'wpb' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	
	register_sidebar( array(
		'name' =>__( 'Various social links', 'wpb'),
		'id' => 'social_links',
		'description' => __( 'Social links', 'wpb' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	
	
}
add_action( 'widgets_init', 'wpb_widgets_init' );






/**
 *	Return parent pages
 */
function om_get_parent_pages($post_type='page'){
	$args = array(	
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'post_type '=> $post_type,
		'post_status' => 'publish',
		'posts_per_page' => -1, 
		'parent' => 0,
	);
	$pages = get_pages($args);
	/*print '<pre>';
	print_r($pages);
	print '</pre>';*/
	return $pages;
}
/**
 *	Return child pages of $parent_id
 */
function om_get_child_pages($parent_id,$post_type='page'){
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'child_of' => $parent_id,
		'parent' => $parent_id
	);
	$pages = get_pages($args);
	/*print '<pre>';
	print_r($pages);
	print '</pre>';*/
	return $pages;
}
/**
 *	Return top level parent
 */
function om_get_1st_parent($id){
	$parent = array_reverse(get_post_ancestors($id));
	return get_page($parent[0]);
}
/**
 *	Return second level parent
 */
function om_get_2nd_parent($id){
	$parent = array_reverse(get_post_ancestors($id));
	return get_page($parent[1]);
}
/**
 *	Return
 */
function om_get_parent($id){
	$parent = get_post_ancestors($id);
	return get_page($parent[0]);
}
/**
 *	Return
 */
function om_get_gparent($id){
	$parent = get_post_ancestors($id);
	return get_page($parent[1]);
}
/**
 *	Return breadcrumb
 */
function get_breadcrumb($type='page') {
	$arrow = ' <span class="arrow">&raquo;</span> ';
	$trail = '<div class="crumbs"><a href="/">Home</a>'.$arrow;
	
	// PAGES
	if ($type == 'page'){
		global $post;
		// if there is a parent
		if($post->post_parent) {
			$parent_id = $post->post_parent;
			
			while ($parent_id) {
				$page = get_page($parent_id);
				$link = '<a href="'.get_permalink($page->ID).'">'.get_the_title($page->ID).'</a>'.$arrow;
				$breadcrumbs[] = $link;
				// look for a parent of the parent on the next loop
				$parent_id = $page->post_parent;
			}
			// flip array to traverse as a hierarchy
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach($breadcrumbs as $crumb) $trail .= $crumb;
		}
		$trail .= '<a href="'. get_page_link($post->ID) .'">' . get_the_title($post->ID) . '</a>';
	} 
	// POSTS
	else if ($type == 'post'){
		global $post;
		$trail .= '<a href="/news">News / Events</a>'.$arrow;
		$trail .= '<a href="'. get_page_link($post->ID) .'">' . get_the_title($post->ID) . '</a>';
	}
	// CATEGORIES
	else if ($type == 'category'){
		$trail .= 'Category'.$arrow;
		$trail .= single_cat_title( '', false );
	}
	
	
	return $trail .'</div>';	
}







/**
 *	Get rid of shortcodes in excerpts
 */
function custom_excerpt($text = '') {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content('');
                // $text = strip_shortcodes( $text );
		$text = do_shortcode( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$excerpt_length = apply_filters('excerpt_length', 55);
		$excerpt_more = apply_filters('excerpt_more', ' ');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
remove_filter( 'get_the_excerpt', 'wp_trim_excerpt'  );
add_filter( 'get_the_excerpt', 'custom_excerpt'  );



function shorten_post_content($content,$chars=180){	
	// strip tags and allow only so many characters
	$htmlout = '';
	$htmlout = str_replace("&nbsp;", "", substr(strip_tags(strip_shortcodes($content)), 0, $chars));
        
	return $htmlout;
}



/**
 *	Shortcode for box
 */
function sBox($atts, $content = null) {
   extract(shortcode_atts(array('link' => '#'), $atts));
   return '<div class="box">' . do_shortcode($content) . '</div>';
}
add_shortcode('box', 'sBox');





function print_subnav($post){
	
	$str = '<div class="page-left-nav">';
	

	/**
	 *	Build the sub navigation
	 */
	
	// 1. get family
	//$family = array_reverse(get_post_ancestors($post->ID));
	//if (isset($family[0])) $family_parent = get_post($family[0]);
	//if (isset($family[1])) $family_gparent = get_post($family[1]);

	// first parent
	$first_parent = om_get_1st_parent($id);
	
//print 'This page: $post->ID = '.$post->ID.'<br><br><br>';
	
	// 2. get children
	if ($children = om_get_child_pages($first_parent->ID,'page')){
		// then make a new list
		$str .= '<ul class="page-subnav">';
		foreach($children as $child){ 
/*
print '$child->ID = '.$child->ID.'<br>';
print '$post->post_parent = '.$post->post_parent.'<br>';

print '<br>';
*/
			//3. create child links
			
			// if the parent of this list is selected (make it red)
			if ($post->ID == $child->ID){
				$str .= '<li><a class="active"';
			// if a child of this list is selected (make it just darker)
			} else if($post->post_parent == $child->ID){
				$str .= '<li><a class="active_gchild"';
			// or do nothing to presentation	
			} else {
				$str .= '<li><a';
			}
			$str .= ' href="'.get_page_link($child->ID).'">'.$child->post_title;
			
			// 4. get grand children
			$gchildren = om_get_child_pages($child->ID,'page');
			
			if ($gchildren){
				// add caret
				$str .= ' <b class="caret"></b>';
			}
			$str .= '</a>';
				
			// is this the active page or child of the active page
			if ($child->ID == $post->ID || $post->post_parent == $child->ID){
					
				if ($gchildren){
					
					// create a sub sub nav
					$str .= '<ul class="page-subsubnav">';
					foreach($gchildren as $gchild){
/*								
print '... $child->ID = '.$child->ID.'<br>';
print '... $gchild->ID = '.$gchild->ID.'<br>';
print '<br>';
*/
						// if a child of this list is selected (make it red)
						if ($post->ID == $gchild->ID){
							$str .= '<li><a class="active"';
						} else {
							$str .= '<li><a';
						}
						$str .= ' href="'.get_page_link($gchild->ID).'">'.$gchild->post_title.'</a>';
						$str .= '</li>';
					}
					$str .= '</ul>';
				}
			}
			
			$str .= '</li>';
		}
		$str .= '</ul>';
	}
	$str .= '</div>';
	
	print $str;
}

/**
 *	Return post categories in a variety of ways
 */
function get_post_categories($post,$format='array',$data='id'){
	$arr = array();
	$str = '';
        
        
        // if $post is null (meaning post is general page with no defined categories by user (like home page),
        // get News categories 
        if ($post == NULL) {
            
            $catid1 = get_cat_ID( "News" );
            $catid2 = get_cat_ID( "news" );
            
            $categories = array(
                get_category( $catid1 ), 
                get_category( $catid2 ));
        }
        
        // else, get all categories specified for the post
        else {        
            $categories = get_the_category($post->ID);
        }
            
	foreach ( $categories as $category ) {
		if ($data == 'id') $data = $arr[$category->slug] = $category->cat_ID;
		else if ($data == 'slug') $arr[$category->cat_ID] = $category->slug;
		else if ($data == 'name') $arr[] = $category->cat_name;
	}
	//print_r($arr);
	if (!empty($arr)) {
		if ($format == 'array'){
			return $arr;
		} else if ($format == 'string'){
			return implode(",", $arr); 
		}
	}
}
/**
 *	Return post category id from a slug
 */
function get_category_id ($slug){
	$term = get_term_by('slug',strtolower($slug),'category');
	return $term->term_id;
}


/**
 *	Display a post feed on template pages
 *
 *	@param $post Object The page calling the template
 *	@param $count Int Number of posts to display
 */
function display_post_feed($post,$count){
	
	// parse url to determine pagination path
	$array = explode('/', $_SERVER['REQUEST_URI']);
	// loop through until page is found
	$path = '';
	foreach($array as $val){
		if ($val == 'page') break;
		if (!empty($val)) $path .= '/'.$val;
	}
	
	// storing / reporting
	$vars = array();
	$vars['path'] = $path;
	// all slugs for the page
	$vars['slug'] = get_post_categories($post,'string','slug');
	// Get categories for the page which is including 
	// this template in order to show specific feeds
	$vars['include'] = get_post_categories($post,'string','id');
        
	// exclude drafts et al
	$vars['exclude'] = get_category_id ('draft'); 
	// variable to determine what number page we're on in this feed
	$vars['paged'] = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	/*
	print '<pre>';
	print_r($vars);
	print '</pre>';
	*/
	// create args for get_posts()
	$args = array(
		'posts_per_page'   => $count,
		//'category'         => 'news,-16',
		'category'         => $vars['include'],
		'exclude'          => $vars['exclude'],
		'paged'            => $vars['paged'],
		'orderby'          => 'post_date',
		'post_status'      => 'publish',
	);
	$posts = get_posts($args);
	if ($posts) {
		// loop through posts
		foreach($posts as $post) {
			// setup global post data
			//setup_postdata($post); // don't believe we need this anymore
	?>
	<div class="row">
	<div class="col-sm-6 col-md-4"><?php echo display_post_img($post); ?></div>
	<div class="col-sm-6 col-md-8">
		<h4><a href="<?php print get_permalink($post); ?>"><?php print $post->post_title; ?></a></h4>
		<div class="feed-date-tags">
			<?php 
				//the_time('F j, Y');
                                echo get_the_time('F j, Y', $post->ID); 
				echo createFeedCategoryLinks($post);
			?>
			</div>
			<?php
			
			if ($post->post_excerpt) {
				// if there is an excerpt then display it
				$htmlout = shorten_post_content($post->post_excerpt,180);
			} else {
				// otherwise use content but remove images
				$htmlout = shorten_post_content($post->post_content,180);
			}
			
			echo $htmlout;
			?>
			... <a href="<?php print get_permalink($post); ?>">Continue reading &raquo;</a>
	</div><!-- /.col-md-8 -->
	</div><!-- /.row -->
	<?php
		}
	} else { print "No posts found."; }
	?>
	<div class="row feed-col-footer"><?php echo om_pagination($post,$path); ?></div>
	<?php 
}
/**
 *	Return html to display list of category links in a feed
 */
function createFeedCategoryLinks($post){
	$post_categories = wp_get_post_categories($post->ID);
	if (count($post_categories) > 0) $str = ' <span class="del">&bull;</span> ';
	foreach($post_categories as $c){
		$cat = get_category( $c );
		if ($cat->slug != 'uncategorized' && $cat->slug != 'draft'){  
			$str .= ' <a class="" href="/category/'. $cat->slug .'"';
			$str .= ' title="More posts in '. $cat->name .'">';
			$str .= strtolower($cat->name) .'</a> ';
		}
	}
	return $str;
}
/**
 *	Return html to display a post image in a feed
 */
function display_post_img($post){	
	//class="img-responsive" ????
	
	$w = 200;
	$h = 150;
	
	// start link to image
	$str = '<a href="'. get_permalink( $post->ID ) .'" title="' . esc_attr( $post->post_title ) . '">';
	
	// check for set featured image
	if ( strlen( get_the_post_thumbnail($post->ID) ) ) {
		$str .= get_the_post_thumbnail($post->ID,'post-thumbnail');
	} 
	// if no featured image, get first image in post
	else if ( preg_match_all( '|<img.*?src=[\'"](.*?)[\'"].*?>|i', $post->post_content, $matches )){
		if ( isset( $matches ) )
			$imageURL = $matches[1][0];
			
		if ( $imageURL ) {
			// reset $str and add style
			$str = '<a href="'. get_permalink( $post->ID ) .'" title="' . esc_attr( $post->post_title );
			$str .= '" style="display:inline-block; overflow:hidden; width:'.$w.'px;height:'.$h.'px">';
			$str .= '<img alt="'.esc_attr( $post->post_title ).'" src="' . $imageURL .'"';
			$str .= ' style="width:250px;"';
			$str .= ' class="attachment-post-thumbnail wp-post-image feed-image-post-thumbnail">';
		}
	} 
	// no image found anywhere
	else {
		$str .= '<div style="width:'.$w.'px;height:'.$h.'px; background:#eee;"';
		$str .= ' class="feed-img-post-thumbnail"></div>';
	}
	
	$str .= '</a>';
	return $str;
}
/**
 *	Return featured image address
 */
function display_featured_post_img($post){
    
        $post_thumbnail_url = "";
        
        $post_thumbnail_id = get_post_thumbnail_id($post->ID);
        $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
	
	return $post_thumbnail_url;
}
/**
 *	Display pagination links
 *
 *	@param $path String The path for the url
 */
function om_pagination($post,$path) {
	
	$include = get_post_categories($post,'string','id');
	// exclude drafts et al
	$exclude = get_category_id ('draft'); 
	// custom query to get all the posts in category
	$wp_query = new WP_Query(array(  
		'cat' => $include, 
		'category__not_in' => $exclude, 
		'posts_per_page' => 10  
	));
	
	
	//print_r($wp_query);
	$pages_to_show = 7;
	$str = '';
	 
	$total_pages = $wp_query->max_num_pages;
	
	//if ($total_pages > 1){
		$current_page = max(1, get_query_var('paged'));
		$count = 0;
		$previous_page = $current_page - 1;
		$next_page = $current_page + 1;
		
		$str .= '<ul class="pagination">';
		
		if ($total_pages > 3) { 
			if ($current_page > 1) {
				$str .= '<li class="last">';
				$str .= '<a href="'. get_bloginfo('url') .$path.'/page/1/?"><<</a></li>' ;
			}
			if ($current_page > 1) {
				$str .= '<li class="previous">';
				$str .= '<a href="'. get_bloginfo('url') .$path.'/page/'. $previous_page .'/?">';
				$str .= '<</i></a></li>' ;
			}
		}
		while ($count < $pages_to_show) {
			$count = $count + 1;  
			
			if ($count == $current_page) {
				$str .= '<li class="active">';
				$str .= '<a href="' . get_bloginfo('url') .$path.'/page/' . $count . '/?">';
				$str .= $count . '</a></li>' ;
			} else {
				$str .= '<li class="inactive">';
				$str .= '<a href="' . get_bloginfo('url') .$path.'/page/' . $count . '/?">';
				$str .= $count . '</a></li>' ;
			}
		}
		if ($total_pages > 3) {
			if ($current_page < $total_pages) {
				$str .= '<li class="next">';
				$str .= '<a href="'. get_bloginfo('url') .$path.'/page/'. $next_page .'/?pp=1">';
				$str .= '></i></a></li>' ;
			}
			if ($current_page < $total_pages) {
				$str .= '<li class="last">';
				$str .= '<a href="'. get_bloginfo('url') .$path.'/page/'. $total_pages .'/?pp=1">';
				$str .= '>></a></li>';
			}
		}
		
		$str .= '</ul>';
	//}
	return $str;
}
	


#####################################################
#													#
#		THIS AREA FOR UNIT-SPECIFIC FUNCTIONS		#
#													#
#####################################################

function social_links( $postid = NULL, $queryStr = NULL ){
        
    $str = array();
    
    // share link of article/post to social media
    if ( $postid != NULL ) {
        
        $href = "";
        $title = "";
        
        // if events, use permelink plus the query post string, to pass the specific item id to the page link
        if ( $queryStr != NULL && (get_post_meta( $postid, '_wp_page_template', true ) == "page-single-event.php") ) {
            $href = get_permalink( $postid ) . "?eventid=" . $queryStr;
            $title = $_SESSION['event']->getProperty("title");
            $caption = shorten_post_content($_SESSION['event']->getProperty("event description"),180);
            $imageurl = $_SESSION['event']->getProperty("event image");
            $date_ = "Event Date: " . date( 'F j', strtotime($_SESSION['event']->getProperty("start time"))) . " to " . date('F j, Y', strtotime($_SESSION['event']->getProperty("end time")));
        }
            
        // if a regular wp page/post, no need to use URL post variavle
        else {
            $href = get_permalink( $postid );
            $title = get_the_title($postid);
        }
        
        
        $str[] = '<a href="https://www.facebook.com/dialog/feed?app_id=145634995501895&display=popup&description=' .$date_ . '&caption=' . $caption . '&name=' . $title . '&link=' . $href . '&picture=' . $imageurl . '&redirect_uri=https://developers.facebook.com/tools/explorer" title="facebook" target="_blank"><i class="fa fa-facebook fa-1x"></i></a>';
        $str[] = '<a href="http://twitter.com/share?url=' . $href . '" title="twitter" target="_blank"><i class="fa fa-twitter fa-1x"></i></a>';
        $str[] = '<a href="http://www.linkedin.com/shareArticle?mini=true&url=' . $href . '&title=' . $title . '" title="linkedin" target="_blank"><i class="fa fa-linkedin fa-1x"></i></a>';
        $str[] = '<a href="http://new.cvatd.fsu.edu/feed/" title="RSS" target="_blank"><i class="fa fa-rss fa-1x"></i></a>';
        $str[] = '<a href="http://new.cvatd.fsu.edu/connect/monthly-newsletter/" title="envelope"><i class="fa fa-envelope fa-1x"></i></a>';

    }
    
    // show link of general cvatd social media pages
    else {
        if (get_theme_mod( 'facebook_uri' )) 
            $str[] = '<a href="' . get_theme_mod( 'facebook_uri' ) . '" title="facebook" target="_blank"><i class="fa fa-facebook fa-1x"></i></a>';
        
        if (get_theme_mod( 'twitter_uri' )) 
            $str[] = '<a href="' . get_theme_mod( 'twitter_uri' ) . '" title="twitter" target="_blank"><i class="fa fa-twitter fa-1x"></i></a>';
        
        if (get_theme_mod( 'linkedin_uri' )) 
            $str[] = '<a href="' . get_theme_mod( 'linkedin_uri' ) . '" title="linkedin" target="_blank"><i class="fa fa-linkedin fa-1x"></i></a>';
        
        $str[] = '<a href="' . get_site_url() . '/rss" title="RSS" target="_blank"><i class="fa fa-rss fa-1x"></i></a>';
        
        if (get_theme_mod( 'newsletter_uri' )) 
            $str[] = '<a href="' . get_theme_mod( 'newsletter_uri' ) . '" title="envelope"><i class="fa fa-envelope fa-1x"></i></a>';

        //$str[] = '<a href="#" title="youtube"><i class="fa fa-youtube fa-1x"></i></a>';
        //$str[] = '<a href="#" title="flickr"><i class="fa fa-flickr fa-1x"></i></a>';
    }

    return implode("\n",$str);
}

// add lightbox to plain images
add_filter('the_content', 'add_lightbox');
function add_lightbox($content) {
       global $post;
       
       // replace images linking to the same image with rel attribute
       //$pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
       $pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
       $replacement = '<a$1href=$2$3.$4$5 rel="lightbox" title="'.$post->post_title.'"$6>$7</a>';
       $content = preg_replace($pattern, $replacement, $content);
       
       return $content;
}

// add lightbox to gallery images
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) )
			$attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default gallery template.
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery'));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$icontag = tag_escape($icontag);
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) )
		$itemtag = 'dl';
	if ( ! isset( $valid_tags[ $captiontag ] ) )
		$captiontag = 'dd';
	if ( ! isset( $valid_tags[ $icontag ] ) )
		$icontag = 'dt';

	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>";
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		if ( ! empty( $link ) && 'file' === $link )
			$image_output = wp_get_attachment_link( $id, $size, false, false );
		elseif ( ! empty( $link ) && 'none' === $link )
			$image_output = wp_get_attachment_image( $id, $size, false );
		else
			$image_output = wp_get_attachment_link( $id, $size, false, false );

		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) )
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';

		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
				$image_output
			</{$icontag}>";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
		if ( $columns > 0 && ++$i % $columns == 0 )
			$output .= '<br style="clear: both" />';
	}

	$output .= "
			<br style='clear: both;' />
		</div>\n";

	return $output;
}


function theme_getEvent() {
	
	include get_template_directory() . "/assets/events/events.php";
	include_once get_template_directory() . "/assets/events/parsing_functions.php";
	$themesUrl = get_template_directory_uri() . "/assets/events/";
        
	$eventid = 0;
	
	// get eventid from url querystring
	if ( isset( $_GET['eventid'] ) )
		$eventid = intval($_GET['eventid']);
		
	// connect to db, query db with eventid, store data into event
	$event = getEvent( $eventid );
        
        return $event;
}


?>