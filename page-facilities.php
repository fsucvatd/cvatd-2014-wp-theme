<?php
/**
   Template Name: Facilities Maps
 * The Template for displaying all facilities & performance spaces.
 */
get_header(); ?>

<div class="container top-container">


	<div class="row">
		
			
			<?php echo get_breadcrumb(); ?>
			<h2><?php print get_the_title($post->ID); ?></h2>
			

			<?php include get_template_directory() . "/assets/maps/index.php"; ?>
		
  

  </div>
</div>

<?php get_footer(); ?>